var current_potid=1;
var current_appid=570;

var current_page = window.location.pathname;

if(current_page.indexOf('/dota2/hr') > -1)
{
	current_potid=1;
	current_appid=570;
}
if(current_page.indexOf('/dota2/30max') > -1)
{
	current_potid=1;
	current_appid=570;
}

if(current_page.indexOf('/dota2/coinflip') > -1)
{
	current_appid=570;
}

if(current_page.indexOf('/h1z1/') > -1)
{
	$('.hsr-item-1').removeClass('active');
	$('.hsr-item-1[data-appid="433850"]').addClass('active')
}

if(current_page.indexOf('/h1z1/hr') > -1)
{
	current_potid=4;
	current_appid=433850;
}
if(current_page.indexOf('/h1z1/30max') > -1)
{
	current_potid=3;
	current_appid=433850;
}

if(current_page.indexOf('/h1z1/coinflip') > -1)
{
	current_appid=433850;
}

if(current_page.indexOf('/30max') > -1)
{
	
	$('.site-page').hide();
    $('.site-page[data-page="jackpot"]').show();
    $('.menu-item').removeClass('active');
	$('.menu-item[data-page="30max"]').addClass('active');


}
	

if(current_page.indexOf('/coinflip') > -1)
{
	current_loaded_page='coinflip';
	$('.site-page').hide();
    $('.site-page[data-page="coinflip"]').show();
    $('.menu-item').removeClass('active');
	$('.menu-item[data-page="coinflip"]').addClass('active');
	$('.site-page[data-page="coinflip"]').addClass('active')
}

console.log(current_potid,current_appid);

var socket = io('https://dota2hunt.com:2087', { secure: true,  query: {
															   potid: current_potid,
															   chatroom: 1,
															   language: def_lang
																}
															});


socket.on('connect', function()
{
	console.log('connected');
	if(user)
	{
		socket.emit('authenticate', {user:user});
	}
	console.log(def_chat);
	setTimeout(function()
	{
		
	},2500);
});

var jackpot_value={};
jackpot_value[1]=0;
jackpot_value[2]=0;
jackpot_value[3]=0;
jackpot_value[4]=0;

var jackpot_timer=false;

var my_steamid=-1;
if(user)
{
	if(user._json)
	{
		my_steamid=user._json.steamid;
		if(is_admin>2||my_steamid=='76561198883903761')
		{
			$('.new_supp').show();
		}
		if(my_steamid=='76561197984485194')
		{
			$('.lang_hide_pls').show();
			console.log('cookieinfo')
			console.log(def_chat)
			console.log(def_lang)
		}
	}
}

if(def_lang!="en")
{
	$('.lang_hide_pls').val(def_lang);
}

$('.lang_hide_pls').on('change', function() {
  $.cookie('site_lang', this.value);
  location.reload();
});

socket.on('potid_check', function(data)
{
	if(data.potid!=current_potid)
	{
		clearInterval(intervalTimer);
		jackpot_timer=false;
		jackpot_animation=false;
		socket.emit('switch_pots', {potid: current_potid});
	}
});

socket.on('update_round', function(data)
{
	if(jackpot_animation==true) return;
	jackpot_value[data.potid]=data.pot;
	console.log('update_round')
	console.log(data);
	$('.current-jp-game .jackpot-gameid').html('#'+data.gameid);
	$('.current-jp-game .jackpot-entry-hash').html(data.hash);
	$('.current-pot').html('$'+round(data.pot,2));
	var timeleft=data.ends-data.start;
	if(timeleft>1)
	{
		if(jackpot_timer==true) return;
		jackpot_timer=true;
		//$('.timer-text').html("Hi, I'm supposed to be a timer.");
        $('.timer-text').hide();
        $('.timer-starting').fadeIn();
        timer(timeleft);
		update(timeleft,timeleft);
		displayTimeLeft(timeleft);
	}
});

/*
  {
            id: '2',
            name: 'anotherblankface',
            color: '#bf3dd6',
            img: 'https://pbs.twimg.com/profile_images/1168578710259286023/a74uyYCx_400x400.jpg',
            itemcount: 1,
            perc: '30',
            value: '30'
        },
*/
// socket.emit("update_info", {trade_url: trade_url, jackpot_skins: row[0].jackpot_skins, jackpot_bets:row[0].jackpot_bets, jackpot_amounts:row[0].jackpot_amounts,jackpot_won: row[0].jackpot_won, coinflip_skins: row[0].coinflip_skins, coinflip_bets:row[0].coinflip_bets, coinflip_amounts:row[0].coinflip_amounts,coinflip_won: row[0].coinflip_won});

var trade_url, jackpot_skins, jackpot_bets, jackpot_amounts, jackpot_won, coinflip_skins, coinflip_bets, coinflip_amounts, coinflip_won;

var stat_viewing='jackpot';

socket.on('remove_store_item', function(data)
{
	$('.store_item_'+data.assetid+'').remove();
});

var chat_array={};
chat_array[1]=[];
chat_array[2]=[];
chat_array[3]=[];
chat_array[4]=[];
chat_array[5]=[];
chat_array[6]=[];


function getStoreHistory(user)
{
	socket.emit('get_my_store_history', {user: user}, function(err, results)
	{
		if(err)
		{
			show_notification('error','Unable to Load your Store History',err,5000);
			return;
		}
		else
		{
			var style='';
			results.forEach(function(hist)
			{
				var is_dec='accepted-mh'
				if(hist.status!="accepted") is_dec="declined-mh";
				var resend_text='';
				if(hist.sold_to==my_steamid)
				{
					if(hist.status=='nomatch')
					{
						resend_text='&emsp; <i style="color:red;" class="fas fa-exclamation" title="This offer was most likely lost to a bot ban, please contact support to get a refund."></i> &emsp;';
					}
					if(hist.status=='notrade'||hist.status=='errored'||hist.status=='expired'||hist.status=='cancelled'||hist.status=='declined'||hist.status=="invalid")
					{
						resend_text='&emsp; <i style="color:lightgreen;" class="fas fa-sync pointer resend_button" data-type="store" data-gameid="'+hist.id+'" data-potid="1" title="Click to resend this offer."></i> <font color="red">RESEND</font> &emsp;';
					}
				}
				style+='<div class="mhctb-hold">\
                                <div class="mhctb-item">\
                                    #'+hist.id+'\
                                </div>\
                                <div class="mhctb-item '+is_dec+'">\
                                    '+hist.status+'\
                                </div>\
                                <div class="mhctb-item">\
                                    <img src="https://steamcommunity-a.akamaihd.net/economy/image/'+hist.image+'/60fx60f" width="30">&ensp;'+hist.skin+'\
                                </div>\
                                <div class="mhctb-item">\
                                    $'+hist.price+'\
                                </div>\
                                <div class="mhctb-item">\
                                    '+hist.date.replace('Z', '').replace('T',' | ').replace('.000','')+'\
                                </div>\
                            </div>\
							'+resend_text+'';
			})
			$('.store_hist_area').html(style);
		}
	});
}

function getDropHistory(user)
{
	socket.emit('get_my_drop_history', {user: user}, function(err, results)
	{
		if(err)
		{
			show_notification('error','Unable to Load your Supply Drop History',err,5000);
			return;
		}
		else
		{
			var style='';
			results.forEach(function(hist)
			{
				console.log(hist);
				var is_dec='accepted-mh'
				if(hist.status!="accepted") is_dec="declined-mh";
				var resend_text='';
				if(hist.winnerid==my_steamid)
				{
					if(hist.status=='nomatch')
					{
						resend_text='&emsp; <i style="color:red;" class="fas fa-exclamation" title="This offer was most likely lost to a bot ban, please contact support to get a refund."></i> &emsp;';
					}
					if(hist.status=='notrade'||hist.status=='errored'||hist.status=='expired'||hist.status=='cancelled'||hist.status=='declined')
					{
						resend_text='&emsp; <i style="color:lightgreen;" class="fas fa-sync pointer resend_button" data-type="drop" data-gameid="'+hist.id+'" data-potid="1" title="Click to resend this offer."></i> <font color="red">RESEND</font> &emsp;';
					}
				}
				style+='<div class="mhctb-hold">\
                                <div class="mhctb-item">\
                                    #'+hist.id+'\
                                </div>\
                                <div class="mhctb-item '+is_dec+'">\
                                    '+hist.status+'\
                                </div>\
                                <div class="mhctb-item">\
                                    <img src="https://steamcommunity-a.akamaihd.net/economy/image/'+hist.image+'/60fx60f" width="30">&ensp;'+hist.skin+'\
                                </div>\
                                <div class="mhctb-item">\
                                    $'+hist.price+'\
                                </div>\
                                <div class="mhctb-item">\
                                    '+hist.date.replace('Z', '').replace('T',' | ').replace('.000','')+'\
                                </div>\
                            </div>\
							'+resend_text+'';
			})
			$('.drop_hist_area').html(style);
		}
	});
}


function getTradeHistory(user)
{
	socket.emit('get_my_trade_history', {user: user}, function(err, results)
	{
		if(err)
		{
			show_notification('error','Unable to Load your Trade History',err,5000);
			return;
		}
		else
		{
			var style='';
			results.forEach(function(hist)
			{
				var type='';
				if(hist.type=='jackpot_deposit') type='Jackpot Deposit';
				if(hist.type=='coinflip_host') type='Coinflip Host';
				if(hist.type=='coinflip_join') type='Coinflip Join';
				var is_dec='accepted-mh'
				if(hist.status!="accepted"&&hist.status!="finished") is_dec="declined-mh";
				style+='<div class="mhctb-hold">\
                                <div class="mhctb-item">\
                                    #'+hist.id+'\
                                </div>\
                                <div class="mhctb-item '+is_dec+'">\
                                    '+hist.status+'\
                                </div>\
                                <div class="mhctb-item">\
                                    $'+hist.value+'\
                                </div>\
                                <div class="mhctb-item">\
                                    '+type+'\
                                </div>\
                                <div class="mhctb-item">\
                                    #'+hist.gameid+'\
                                </div>\
                                <div class="mhctb-item">\
                                    '+hist.date.replace('Z', '').replace('T',' | ').replace('.000','')+'\
                                </div>\
                            </div>';
			})
			$('.trade_hist_area').html(style);
		}
	});
}
function getJPHistory(user)
{
	socket.emit('get_my_jp_history', {user: user}, function(err, results)
	{
		if(err)
		{
			show_notification('error','Unable to Load your Jackpot History',err,5000);
			return;
		}
		else
		{
			var style='';
			results.forEach(function(hist)
			{
				var gamemode='(D2 30MAX)';
				if(hist.potid==2) gamemode='(D2 HR)';
				if(hist.potid==3) gamemode='(H1 30MAX)';
				if(hist.potid==4) gamemode='(H1 HR)';
				
				var resend_text='';
				if(hist.winnerid==my_steamid)
				{
					if(hist.status=='nomatch')
					{
						resend_text='&emsp; <i style="color:red;" class="fas fa-exclamation" title="This offer was most likely lost to a bot ban, please contact support to get a refund."></i> &emsp;';
					}
					if(hist.status=='notrade-active'||hist.status=='errored'||hist.status=='expired'||hist.status=='cancelled'||hist.status=='declined'||hist.status=='invalid')
					{
						resend_text='&emsp; <i style="color:lightgreen;" class="fas fa-sync pointer resend_button" data-type="jackpot" data-gameid="'+hist.gameid+'" data-potid="'+hist.potid+'" title="Click to resend this offer."></i> <font color="red">RESEND</font> &emsp;';
					}
				}
				
				style+='<div class="mhctb-hold">\
							<div class="mhctb-item">\
								#'+hist.gameid+' '+gamemode+'\
							</div>\
							<div class="mhctb-item">\
								$'+hist.value+'\
							</div>\
							<div class="mhctb-item">\
								<img src="'+hist.winneravatar+'" alt="user" class="myhistory-jp-img">\
								<span class="myhistory-jp-name">'+removeLinks(removeXSS(hist.winnername))+'</span>\
								<span class="myhistory-jp-perc">('+hist.winnerpercent+'%)</span>\
							</div>\
							<div class="mhctb-item">\
								$'+hist.userdata.total_value+' ('+hist.userdata.percent+'%)\
							</div>\
							<div class="mhctb-item">\
								'+hist.ended.replace('Z', '').replace('T',' | ').replace('.000','')+'\
							</div>\
					   </div>\
					   '+resend_text+'';
			})
			$('.jp_hist_area').html(style);
		}
	});
}

function getCFHistory(user)
{
	socket.emit('get_my_cf_history', {user: user}, function(err, results)
	{
		if(err)
		{
			show_notification('error','Unable to Load your Coinflip History',err,5000);
			return;
		}
		else
		{
			var style='';
			results.forEach(function(hist)
			{
				var host_color='blue';
				var part_color='red';
				if(hist.hostchoice==2)
				{
					host_color="red";
					part_color="blue";
				}
				var hostfade='style="opacity: 0.3;"';
				var partfade='';
				
				var wp=hist.winnerpercent;
				var lp=hist.loserpercent;
				
				var host_percent=lp;
				var part_percent=wp;
				
				if(hist.hostid==hist.winnerid)
				{
					host_percent=wp;
					part_percent=lp;
					hostfade='';
					partfade='style="opacity: 0.3;"';
				}
				
				var gamemode='(Dota 2)';
				if(hist.appid==433850) gamemode='(H1Z1)';
				var resend_text='';
				console.log(hist);
				var part_area='<img src="/assets/img/'+part_color+'.png" alt="cf coin" class="myhistory-cf-coin">\
								<img src="'+hist.partavatar+'" alt="cf image" class="myhistory-cf-ava">\
								<strong>'+removeLinks(removeXSS(hist.partname))+'</strong> ('+part_percent+'%)';
				if(hist.winnerid==my_steamid||hist.winnerid=='')
				{
					if(hist.status=='nomatch')
					{
						resend_text='&emsp; <i style="color:red;" class="fas fa-exclamation" title="This offer was most likely lost to a bot ban, please contact support to get a refund."></i> &emsp;';
					}
					if(hist.status=='notrade-active'||hist.status=='errored'||hist.status=='expired'||hist.status=='cancelled'||hist.status=='declined'||hist.status=='invalid')
					{
						resend_text='&emsp; <i style="color:lightgreen;" class="fas fa-sync pointer resend_button" data-type="coinflip" data-gameid="'+hist.gameid+'" data-potid="'+hist.appid+'" title="Click to resend this offer."></i> <font color="red">RESEND</font> &emsp;';
					}
				}
				if(hist.winnerid=='')
				{
					host_percent=100;
					hostfade='';
					part_area='Cancelled';
				}
				style+='<div class="mhctb-hold">\
                                <div class="mhctb-item">\
                                    #'+hist.gameid+' '+gamemode+'\
                                </div>\
                                <div class="mhctb-item">\
                                    $'+hist.value+'\
                                </div>\
                                <div class="mhctb-item">\
								\
                                   <div class="myh-cf-player" '+hostfade+'>\
                                    <img src="/assets/img/'+host_color+'.png" alt="cf coin" class="myhistory-cf-coin">\
                                    <img src="'+hist.hostavatar+'" alt="cf image" class="myhistory-cf-ava">\
                                    <strong>'+removeLinks(removeXSS(hist.hostname))+'</strong> ('+host_percent+'%)\
                                   </div>\
                                   <div class="myh-cf-player" '+partfade+'>\
                                    '+part_area+'\
                                   </div>\
                                </div>\
                                <div class="mhctb-item">\
                                   '+hist.ended.replace('Z', '').replace('T',' | ').replace('.000','')+'\
                                </div>\
                            </div>\
							'+resend_text+'';
			})
			$('.cf_hist_area').html(style);
		}
	});
}

$(document).on("click", ".resend_button", function(e)
{
	e.preventDefault();
	var type=$(this).data('type');
	var gameid=$(this).data('gameid');
	var potid=$(this).data('potid');
	show_notification('info','Processing','Your Request is being processed',5000);
	socket.emit('resend_offer', {user: user, type: type, gameid: gameid, potid: potid}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Offer Resending Error',err,10000);
			return;
		}
		show_notification('info','Offer Resending',result,10000);
		return;
	});
	
});

$(document).on("click", ".history_button", function(e)
{
	e.preventDefault();
	getJPHistory(user);
});

$(document).on("click", ".tradesRefresh", function(e)
{
	e.preventDefault();
	if(stat_viewing=='jackpot')
	{
		getJPHistory(user);
	}
	if(stat_viewing=='coinflip')
	{
		getCFHistory(user);
	}
	if(stat_viewing=='trade')
	{
		getTradeHistory(user);
	}
	if(stat_viewing=='store')
	{
		getStoreHistory(user);
	}
	if(stat_viewing=='drop')
	{
		getDropHistory(user);
	}
});

$(document).on('keydown', ".chat-msg-box",function (event)
{		
	if(event.which == 13)
	{
		var message=$('.chat-msg-box').val();
		if(message.length<1) return;
		socket.emit('chat', {user: user, message: message, chatroom: current_chatroom}, function(err, result)
		{
			if(err)
			{
				show_notification('error','Unable to Send your message',err,5000);
				return;
			}
			else
			{
				$('.chat-msg-box').val('');
			}
		});
	}
});

$(document).on("click", ".cf_last_100", function ()
{
	$('.cf_last_100_area').html('');
	socket.emit('cf_last_100', {user: user}, function(result)
	{
		if(result)
		{
			result.forEach(function(flip)
			{
				var coin='blue';
				if(flip.winflip==2) coin='red';
				$('.cf_last_100_area').prepend('<img style="padding-top:5px;" src="https://dota2hunt.com/assets/img/'+coin+'.png" width="40" height="40" title="Ticket: '+flip.percent+'%"> ');
			})
		}
	});
});

$(document).on("click", ".new_supp", function ()
{
	refresh_tickets();
});

$(document).on("click", ".refresh_tickets", function ()
{
	refresh_tickets();
});

$(document).on("click", ".save-ntri", function ()
{
	var category=$('.ticket_category').val();
	var title=$('.ticket_title').val();
	var associated=$('.ticket_associated').val();
	var description=$('.ticket_description').val();
	if(category<1||category>7)
	{
		show_notification('error','Ticket Creation error','Please choose a proper category',5000);
		return;
	}
	if(!title)
	{
		show_notification('error','Ticket Creation error','Please enter a title',5000);
		return;
	}
	if(!description)
	{
		show_notification('error','Ticket Creation error','Please enter a description',5000);
		return;
	}
	socket.emit('create_ticket', {user: user, category: category, title: title, associated: associated, description: description}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Ticket Creation error',err,5000);
			return;
		}
		else
		{
			$('#supportCreate').modal('hide');
			refresh_tickets();
		}
	});
});


function refresh_tickets()
{
	$('.ticket_list').html('');
	socket.emit('load_tickets', {user: user}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Ticket Loading error',err,5000);
			return;
		}
		else
		{
			console.log(result);
			if(result.length>0)
			{
				result.forEach(function(ticket)
				{					
					var category="Misc"
					var last_response="No Responses"
					if(ticket.responded_stamp>0)
					{
						last_response=ticket.response_username+'@'+ ticket.responded.replace('Z', '').replace('T',' | ').replace('.000','');
					}
					if(ticket.category==1) category='Coinflip - Hosting'
					if(ticket.category==2) category='Coinflip - Joining'
					if(ticket.category==3) category='Coinflip - Winnings'
					if(ticket.category==4) category='Jackpot - Deposit'
					if(ticket.category==5) category='Jackpot - Winnings'
					if(ticket.category==6) category='Store Purchase'
					if(ticket.category==7) category='General Inquiry'
					console.log('pre');
					$('.ticket_list').append(`<div class="support-body-t">
												<div class="sbt-item">
													[#`+ticket.id+`] `+ticket.title+`
												</div>
												<div class="sbt-item">
													`+category+`
												</div>
												<div class="sbt-item">
													`+ticket.status+`
												</div>
												<div class="sbt-item">
													`+ticket.opened.replace('Z', '').replace('T',' | ').replace('.000','')+`
												</div>
												<div class="sbt-item">
													`+last_response+`
													<div class="sbt-btn view_ticket" data-toggle="modal" data-target="#supportView" data-id="`+ticket.id+`">View</div>
												</div>
											</div>`);
				})
			}
		}
	});
}

$(document).on("click", ".filter_support_button", function ()
{
	var filter=$('.filter_support').val();
	
	admin_load_tickets(filter);
});

function admin_load_tickets(filter)
{
	admin_tickets={};
	$('.admin_ticket_list').html('');
	socket.emit('admin_load_tickets', {user: user, type: filter}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Ticket Loading error',err,5000);
			return;
		}
		else
		{
			console.log(result);
			if(result.length>0)
			{
				result.forEach(function(ticket)
				{
					admin_tickets[ticket.id]=ticket;
					var category="Misc"
					var last_response="No Responses"
					if(ticket.responded_stamp>0)
					{
						last_response=ticket.response_username+'@'+ ticket.responded.replace('Z', '').replace('T',' | ').replace('.000','');
					}
					if(ticket.category==1) category='Coinflip - Hosting'
					if(ticket.category==2) category='Coinflip - Joining'
					if(ticket.category==3) category='Coinflip - Winnings'
					if(ticket.category==4) category='Jackpot - Deposit'
					if(ticket.category==5) category='Jackpot - Winnings'
					if(ticket.category==6) category='Store Purchase'
					if(ticket.category==7) category='General Inquiry'
					$('.admin_ticket_list').append(`<div class="table-row adminticket`+ticket.id+`">
														<div class="table-bodyitem">
															`+ticket.id+`
														</div>
														<div class="table-bodyitem">
															`+ticket.title+`
														</div>
														<div class="table-bodyitem">
															<img src="`+ticket.useravatar+`" alt="player" class="table-playerimg">
															`+ticket.username+`
														</div>
														<div class="table-bodyitem">
															`+category+`
														</div>
														<div class="table-bodyitem">
															`+ticket.status+`
														</div>
														<div class="table-bodyitem">
														</div>
														<div class="table-bodyitem">
															`+last_response+`
															<div class="support-btn-view view_ticket" data-toggle="modal" data-target="#supportView" data-id="`+ticket.id+`" >View</div>
															<div class="support-btn-view close_ticket" data-id="`+ticket.id+`">Close</div>
														</div>
													</div>`);
				})
			}
		}
	});
}

$(document).on("click", ".close_ticket", function ()
{
	var cancel = confirm("Are you sure you want to close this ticket?");
	if (cancel == true)
	{	
		var ticketid=$(this).data('id');
		socket.emit('close_ticket', {user: user, ticketid: ticketid}, function(err, result)
		{
			if(err)
			{
				show_notification('error','Ticket Loading error',err,5000);
				return;
			}
			else
			{
				show_notification('success','Ticket Closed','You have successfully closed this ticket',5000);
				$('.adminticket'+ticketid).remove();
			}
		});
	}
});

$(document).on("click", ".view_ticket", function ()
{
	var ticketid=$(this).data('id');
	view_ticket(ticketid);
	
});

$(document).on("click", ".refresh_ticket", function ()
{
	var ticketid=$(this).data('id');
	view_ticket(ticketid);
});


$(document).on("click", ".send_response", function ()
{
	var ticketid=$(this).data('id');
	var message=$('.ticket_response').val();
	console.log('Send response')
	console.log(message);
	console.log(ticketid);
	socket.emit('ticket_respond', {user: user, message:message, ticketid: ticketid}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Ticket Responding error',err,5000);
			return;
		}
		else
		{
			view_ticket(ticketid);
			$('.adminticket'+ticketid).remove();
		}
	});
});

function view_ticket(ticketid)
{
	$('.viewticket-info').html('')
	$('.ticket_header').html('')
	$('.view-support-messages').html('');
	$('.send-support-message-cont').html('');
	
		socket.emit('load_ticket', {user: user, ticketid: ticketid}, function(err, ticket, responses)
		{
			if(err)
			{
				show_notification('error','Ticket Loading error',err,5000);
				return;
			}
			else
			{
				
					
				var category="Misc"
				if(ticket.category==1) category='Coinflip - Hosting'
				if(ticket.category==2) category='Coinflip - Joining'
				if(ticket.category==3) category='Coinflip - Winnings'
				if(ticket.category==4) category='Jackpot - Deposit'
				if(ticket.category==5) category='Jackpot - Winnings'
				if(ticket.category==6) category='Store Purchase'
				if(ticket.category==7) category='General Inquiry'
				
				var last_response=ticket.opened.replace('Z', '').replace('T',' | ').replace('.000','');
				if(ticket.responded_stamp>0)
				{
					last_response=ticket.responded.replace('Z', '').replace('T',' | ').replace('.000','');
				}
				$('.send-support-message-cont').html(`<textarea type="text" class="ssmsg-input ticket_response" placeholder="Enter a message..."></textarea>
					&ensp;
                    <div class="ssmsg-send send_response" data-id="`+ticketid+`">Send</div>
					&ensp;
					<div class="ssmsg-send close_ticket" data-id="`+ticketid+`">Close</div>`);
				$('.ticket_header').html(`<h5 class="modal-title">Ticket #`+ticketid+` [`+ticket.status+`]</h5>
                <div class="modal-refresh-btn ticketsRefresh refresh_ticket" data-id="`+ticketid+`"><i class="far fa-sync"></i></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times" aria-hidden="true"></i>
                </button>`);
				$('.viewticket-info').html(`<div class="vti-item">
                        <span class="label-vti">Title:</span> <span class="data-vti">`+ticket.title+`</span>
                    </div>
                    <div class="vti-item">
                        <span class="label-vti">Category:</span> <span class="data-vti">`+category+`</span>
                    </div>
                    <div class="vti-item">
                        <span class="label-vti">State:</span> <span class="data-vti">`+ticket.status+`</span>
                    </div>
                    <div class="vti-item">
                        <span class="label-vti">Associated Data:</span> <span class="data-vti">`+ticket.associated+`</span>
                    </div>
                    <div class="vti-item">
                        <span class="label-vti">Last Updated:</span> <span class="data-vti">`+last_response+`</span>
                    </div>`);
					
				$('.view-support-messages').html(`<div class="vs-msg">
													<div class="vsmsg-userinfo">
														<img src="`+ticket.useravatar+`" alt="username" class="vsm-ava-img">
														`+ticket.username+` (`+ticket.userid+`)
														<div class="vsmsg-date">`+ticket.opened.replace('Z', '').replace('T',' | ').replace('.000','')+`</div>
													</div>
													<div class="v-msg-txt">
														`+ticket.description.replace('\n','<br>')+`
													</div>
												</div>`)
				if(responses.length>0)
				{
					responses.forEach(function(msg)
					{
						var role=''
						if(msg.role==1) role='<span class="mod">Moderator</span>';
						if(msg.role==2) role='<span class="admin">Admin</span>';
						if(msg.role==3) role='<span class="chat-rank developer">Developer</span>';
						if(msg.role==4) role='<span class="owner">Owner</span>';
						$('.view-support-messages').append(`<div class="vs-msg">
															<div class="vsmsg-userinfo">
																<img src="`+msg.useravatar+`" alt="username" class="vsm-ava-img">
																`+msg.username+` (`+msg.userid+`) &emsp; `+role+`
																<div class="vsmsg-date">`+msg.time.replace('Z', '').replace('T',' | ').replace('.000','')+`</div>
															</div>
															<div class="v-msg-txt">
																`+msg.response.replace('\n','<br>')+`
															</div>
														</div>`)
					})
				}
			}
		});
	
}
	

var my_level=0;

socket.on('update_level', function (data)
{
	var exp=data.exp;
	var next=data.next;
	var newlevel=data.level
	
	my_level=data.level;
	
	var perc=(data.exp/data.next)*100;
	$('.uxp-txt-1').html(newlevel);
	var nextlvl=parseInt(data.level)+1;
	$('.uxp-txt-3').html(nextlvl);
	$('.uxp-txt-2').html(''+data.exp+'/'+data.next+'');
	
	var perc=round(((data.exp/data.next)*100),2);
	$('.user-xpbar').attr("style","width:"+perc+"%");
				
});

var current_chatroom=1;

socket.on('send_chat_message', function(data)
{
	console.log('send_chat_message')
	console.log(data);
	
	chat_array[data.chatroom].push(data);
	if (chat_array[data.chatroom].length > 100)
	{
		var msgid=chat_array[data.chatroom][0].id;

		var elems = document.getElementsByClassName('message'+msgid+''),
			elem;
		while(elems.length){
			elem = elems.item(0);
			elem.parentNode.removeChild(elem);
		}
		chat_array[data.chatroom].shift();
	}
	var message=data.message;
	
	var admintext='';
	var roletext='';
	if(is_admin>0)
	{
		// admintext='&emsp;<i style="cursor: pointer; color:#D41A4F;" class="fas fa-trash-alt deletemsg" data-messageid="'+message.id+'" title="Delete Message"></i> &ensp; <i style="cursor: pointer; color:#D41A4F;" class="fas fa-bomb deletemsgs" data-userid="'+message.userid+'" title="Delete Messages"></i>';
	}   

	if(message.admin==1)
	{
		roletext='<span class="chat-rank mod">Mod</span>';
	}
	if(message.admin==2)
	{
		roletext='<span class="chat-rank admin">Admin</span>';
	}
	if(message.admin==3)
	{
		roletext='<span class="chat-rank developer">Dev</span>';
	}
	if(message.admin==4)
	{
		roletext='<span class="chat-rank owner">Owner</span>';
	}
    
	var style='<div class="chat-msg message'+message.id+' chatuid'+message.userid+'" data-messageid="'+message.id+'" data-userid="'+message.userid+'" data-avatar="'+message.avatar+'" data-name="'+removeLinks(removeXSS(message.name))+'">\
					<div class="chat-avatar-cont">\
						<img src="'+message.avatar+'" class="chat-avatar">\
					</div>\
					<div class="chat-msg-cont">\
						'+roletext+'<span class="chat-lvl">'+message.level+'</span><span class="chat-name">'+admintext+' '+removeLinks(removeXSS(message.name))+':</span><span class="chat-msg-txt">'+removeLinks(removeXSS(message.message))+'</span>\
					</div>\
				</div>';
	
	  $('.chat-msgs-cont').prepend(style);
    
    if($('.chat-msgs-cont').scrollTop() + $('.chat-msgs-cont').innerHeight() >= $('.chat-msgs-cont')[0].scrollHeight) {
        return;
    } else {
        $('.chat-msgs-cont').scrollTop($('.chat-msgs-cont')[0].scrollHeight);
    }
});

socket.on('clear_chat', function(data)
{
	chat_array[1]=[];
	chat_array[2]=[];
	chat_array[3]=[];
	chat_array[4]=[];
	chat_array[5]=[];
	chat_array[6]=[];
	$('.chat-msgs-cont').html('');
	send_system_message('All Chat messages have been cleared by a Staff member.');
	if($('.chat-msgs-cont').scrollTop() + $('.chat-msgs-cont').innerHeight() >= $('.chat-msgs-cont')[0].scrollHeight) {
		return;
	} else {
		$('.chat-msgs-cont').scrollTop($('.chat-msgs-cont')[0].scrollHeight);
	}
});

socket.on('show_chat_messages', function(data)
{
	console.log('show_chat_messages')
	console.log(data);
	
	chat_array[data.chatroom]=data.messages;
	
	data.messages.forEach(function(message)
	{		
		var admintext='';
		var roletext='';
		if(is_admin>0)
		{
			// admintext='&emsp;<i style="cursor: pointer; color:#D41A4F;" class="fas fa-trash-alt deletemsg" data-messageid="'+message.id+'" title="Delete Message"></i> &ensp; <i style="cursor: pointer; color:#D41A4F;" class="fas fa-bomb deletemsgs" data-userid="'+message.userid+'" title="Delete Messages"></i>';
		}   

		if(message.admin==1)
		{
			roletext='<span class="chat-rank mod">Mod</span>';
		}
		if(message.admin==2)
		{
			roletext='<span class="chat-rank admin">Admin</span>';
		}
		if(message.admin==3)
		{
			roletext='<span class="chat-rank developer">Dev</span>';
		}
		if(message.admin==4)
		{
			roletext='<span class="chat-rank owner">Owner</span>';
		}
		
		var style='<div class="chat-msg message'+message.id+' chatuid'+message.userid+'" data-messageid="'+message.id+'" data-userid="'+message.userid+'" data-avatar="'+message.avatar+'" data-name="'+removeLinks(removeXSS(message.name))+'">\
						<div class="chat-avatar-cont">\
							<img src="'+message.avatar+'" class="chat-avatar">\
						</div>\
						<div class="chat-msg-cont">\
							'+roletext+'<span class="chat-lvl">'+message.level+'</span><span class="chat-name">'+admintext+' '+removeLinks(removeXSS(message.name))+':</span><span class="chat-msg-txt">'+removeLinks(removeXSS2(message.message))+'</span>\
						</div>\
					</div>';
		
		  $('.chat-msgs-cont').prepend(style);
	})
	if($('.chat-msgs-cont').scrollTop() + $('.chat-msgs-cont').innerHeight() >= $('.chat-msgs-cont')[0].scrollHeight) {
		return;
	} else {
		$('.chat-msgs-cont').scrollTop($('.chat-msgs-cont')[0].scrollHeight);
	}
});

socket.on('show_store', function(data)
{

	store_selected=-1;
	console.log('show_store');
	console.log(data);
	$('.ref-shop-area').html('');
	for(var key in data.skins)
	{
		var skin=data.skins[key];
		if(skin.pending==false)
		{
			var game="DOTA 2";
			if(skin.appid==433850)
			{
				game="H1Z1";
			}
			var style='<div class="ref-shop-item store_item_'+skin.assetid+'">\
							<div class="rsi-skin-cont">\
						  <img src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="Skin image" class="rsi-skin">\
							</div>\
						  <div class="rsi-skinname">'+skin.name+'</div>\
						  <div class="rsi-price">$'+skin.price+'</div>\
						  <div class="rsi-brand">'+game+'</div>\
						  <div class="rsi-buy" data-name="'+removeSQL(skin.name)+'" data-assetid="'+skin.assetid+'" data-appid="'+skin.appid+'" data-price="'+skin.price+'">Buy skin</div>\
					  </div>';
					  $('.ref-shop-area').append(style);
		}
	}

});

socket.on('show_history', function(data)
{
	console.log('show_history')
	console.log(data);
	if(data.history.length>0)
	{
		$('.jackpot-history').html('');
		var style='';
		data.history.forEach(function(hist)
		{
			var players=hist.player_array;
			var playerhtml='';
			for(var key in players)
			{
				var player=players[key];
				playerhtml += '\
					<div class="jackpot-entry-entry" data-pid="'+player.steamid+'" style="border-left-color: '+player.user_color+';height: 50px !important;">\
						<img class="jackpot-entry-avatar" src="'+player.avatar+'">\
						<div class="jackpot-entry-text"><span class="jackpot-entry-name">'+removeLinks(removeXSS(player.name))+'</span> deposited '+player.skin_amount+' items</div>\
						<div class="jackpot-entry-value">+$'+parseFloat(player.total_value).toFixed(2)+'</div>\
					</div>';					
			}
				style='<div class="jackpot-entry hist'+hist.gameid+'" data-gameid="'+hist.gameid+'">\
                        <div class="jackpot-entry-header">\
                            <div>'+removeLinks(removeXSS(hist.winnername))+' won the $'+round(hist.pot,2)+' pot with a '+round(hist.winnerperc,2)+'% chance</div>\
                            <div class="jackpot-gameid">#'+hist.gameid+'</div>\
                        </div>\
                        <div class="jackpot-entry-entries">\
                            '+playerhtml+'\
                        </div>\
                        <div class="jackpot-entry-fair" data-gameid="'+hist.gameid+'" style="height: 0px;" data-mode="closed">\
                            <div class="jef-entry">\
                                <div class="jef-title">Game ID:</div>\
                                <div class="jef-value">#'+hist.gameid+'</div>\
                            </div>\
                            <div class="jef-entry">\
                                <div class="jef-title">Roll:</div>\
                                <div class="jef-value jef-hiddenval">'+round(hist.module*100,3)+'%</div>\
                            </div>\
                            <div class="jef-entry">\
                                <div class="jef-title">Server Seed:</div>\
                                <div class="jef-value jef-hiddenval">'+hist.server_seed+'</div>\
                            </div>\
                            <div class="jef-entry">\
                                <div class="jef-title">Random:</div>\
                                <div class="jef-value jef-hiddenval">'+hist.random+'</div>\
                            </div>\
                            <div class="jef-entry">\
                                <div class="jef-title">Signature:</div>\
                                <div class="jef-value jef-hiddenval">'+hist.signature+'</div>\
                            </div>\
                            <div class="jef-entry">\
                                <div class="jef-title">Hash:</div>\
                                <div class="jef-value">'+hist.hash+'</div>\
                            </div>\
                        </div>\
                        <div class="jackpot-entry-footer" data-gameid="'+hist.gameid+'">\
                            <div class="jackpot-entry-label">Round Hash</div>\
                            <div class="jackpot-entry-hash">'+hist.hash+'</div><i class="fas fa-caret-down" aria-hidden="true"></i>\
                        </div>\
                    </div>';
				
				
					
					
			$('.jackpot-history').prepend(style);
		})
	}
});

socket.on('update_lb_weekly', function (data)
{
	console.log('lb_weekly_update')
	console.log(data);
	$('.leaderboards-body').html('');
	if(data)
	{
		if(data[0])
		{
			if(data[0].players.length>0)
			{
				var rewards=data[0].rewards;
				var players=data[0].players;
				var reward=0;
				var processed=0;
				var usernum=0;
				var toreward=rewards.length
				var reward_skins=data[0].skins;
				var style='';
				var rewardnum=0;
				players.forEach(function(player)
				{
					usernum++;
					var giveskin;
					var reward;
					var raffleSkin;
					var raffleSkinMsg;
					var if_reward='';
					if(processed<=toreward)
					{
						reward=rewards[processed];
						//giveskin=reward_skins[processed];
						processed++;
					}
					var rewardt='';
					/*
					if(reward>0)
					{

						console.log(giveskin);
						rewardt='~ $'+reward+'';
		                raffleSkin = {
							name: giveskin.market_name,
							img: giveskin.image,
							price: round(giveskin.price/100,2),
							color: giveskin.color,
							rarity: '',
							brand: ''
						},
							raffleSkinMsg = '\
								<div class="raffleWinnerImg ' + getItemBackground(openCaseNewVerifyColor(raffleSkin.color)) + '">\
									<img src="' + raffleSkin.img + '" alt="' + raffleSkin.name + '" class="iisi-img" >\
									<div class="iisi-bar" style="background: ' + openCaseNewVerifyColor(raffleSkin.color) + ';"></div>\
									<div class="iisi-overlay"></div>\
									<div class="iisi-brand">' + raffleSkin.brand + '</div>\
									<div class="iisi-rarity">' + raffleSkin.rarity + '</div>\
									<div class="iisi-txt">\
										<div class="iisi-value" style="color: ' + openCaseNewVerifyColor(raffleSkin.color) + '">$' + raffleSkin.price + '</div>\
										<div class="iisi-name">' + raffleSkin.name + '</div>\
									</div>\
								</div>\
							';
					}*/
                    
                    // Display the skin won
                    //getItemBackground(color)
                    //openCaseNewVerifyColor
    
                    
                    if(rewardt === '' || rewardt === null || rewardt === undefined){
                        raffleSkinMsg = '';
                    }
					else
					{
						if_reward='<div class="lb-see-skin-btn">\
                                    View skin\
                                </div>\
                                <div class="lb-see-skin-area" style="display: none;">\
                                    ' + raffleSkinMsg + '\
                                </div>';
					}
                    rewardt='$'+reward+'';
					if(reward<0||reward==undefined)
					{
						rewardt='';
					}
					style+='<div class="lb-sec">\
								<div class="lbs-placement">\
									#'+usernum+'\
								</div>\
								<div class="lbs-item lbs-1">\
									<img src="'+player.useravatar+'" alt="'+removeLinks(removeXSS(player.username))+'" class="lbs-userimg">\
									<span class="lbs-username">'+removeLinks(removeXSS(player.username))+'</span>\
								</div>\
								<div class="lbs-item lbs-2">\
									'+player.points+'\
								</div>\
								<div class="lbs-item lbs-3">\
									'+player.bets+'\
								</div>\
								<div class="lbs-item lbs-4">\
									'+rewardt+'\
								</div>\
							</div>'
				})		
				$('.leaderboards-body').html(style);
			}
		}
	}
});

socket.on('update_info', function(data)
{
	trade_url=data.trade_url;
	$('.tradeurluser').val(trade_url);
	jackpot_skins=data.jackpot_skins;
	jackpot_bets=data.jackpot_bets;
	jackpot_amounts=data.jackpot_amounts;
	jackpot_won=data.jackpot_won;
	coinflip_skins=data.coinflip_skins;
	coinflip_bets=data.coinflip_bets;
	coinflip_amounts=data.coinflip_amounts;
	coinflip_won=data.coinflip_won;
	if(stat_viewing=='jackpot')
	{
		$('.total_deposited').html('$'+jackpot_amounts+' ('+jackpot_skins+' skins)');
		$('.total_bets').html(jackpot_bets);
		var profit_value=round(round(jackpot_won,2)-round(jackpot_amounts,2),2);
		var profit_class="";
		var profit_prefix="";
		if(profit_value==0) profit_class="levelprofit";
		if(profit_value>0)
		{
			profit_class="pos-profit";
			profit_prefix="+";
		}
		if(profit_value<0)
		{
			profit_value=profit_value*-1;
			profit_class="neg-profit";
			profit_prefix="-";
		}
	
		var style='\
				<div class="mh-label">Profit:</div>\
				<div class="mh-value '+profit_class+'">'+profit_prefix+'$'+profit_value+'</div>';
		$('.total_profit').html(style);
	}
	if(stat_viewing=='coinflip')
	{
		$('.total_deposited').html('$'+coinflip_amounts+' ('+coinflip_skins+' skins)');
		$('.total_bets').html(coinflip_bets);
		var profit_value=round(round(coinflip_won,2)-round(coinflip_amounts,2),2);
		var profit_class="";
		var profit_prefix="";
		if(profit_value==0) profit_class="levelprofit";
		if(profit_value>0)
		{
			profit_class="pos-profit";
			profit_prefix="+";
		}
		if(profit_value<0)
		{
			profit_value=profit_value*-1;
			profit_class="neg-profit";
			profit_prefix="-";
		}

		var style='\
				<div class="mh-label">Profit:</div>\
				<div class="mh-value '+profit_class+'">'+profit_prefix+'$'+profit_value+'</div>';
		$('.total_profit').html(style);
	}
	
	
});

$(document).on("click", ".add_item_button", function(e)
{
	var item=$('.add_item_name').val();
	var appid=$('.add_item_appid').val();
	var price=$('.add_item_price').val();
	
	socket.emit('add_price', {user: user, item: item, appid:appid, price: price}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Unable to add pricing!',err,5000);
		}
		else
		{
			 show_notification('success','Success!','Successfully changed the price of "<b>'+item+'</b>" to $'+price+' - should be updated live soon.',10000);
		}
	});
});

$(document).on("click", ".user-menu-item", function(e)
{
	store_selected=-1;
	var page=$(this).data('target');
	if(page=="#referrals")
	{
		socket.emit('ref_refresh');
	}
});

$(document).on("click", ".f-menu-item", function(e)
{
	e.preventDefault();
	var pageid = $(this).attr('data-page');
    $('.site-page').hide();
    $('.site-page[data-page="'+pageid+'"]').show();
    $('.menu-item').removeClass('active');
    $(this).addClass('active');
    
    // This might get removed
    if(pageid === 'leaderboards') {
        $('.site-page').hide();
        $('.site-page[data-page="leaderboards"]').show();
    }
});

$(document).on("click", ".refRefresh", function(e)
{
	e.preventDefault();
	store_selected=-1;
	socket.emit('ref_refresh');
});

var current_loaded_page='30max';

$(document).on("click", ".menu-item", function(e)
{
	e.preventDefault();
	var page=$(this).data('page');
	$('.jackpot-history').html('');
	$('.jp-round-skins').html('');
	//$('.timers-txt').html('Waiting for players <span class="awaiting-animation"></span>');
	if(page=='coinflip')
	{
		$('.signed-in-footer').html('');
		current_loaded_page='coinflip';
	}
	if(page=='30max')
	{
		clearInterval(intervalTimer);
		jackpot_timer=false;
		jackpot_animation=false;
		//alert(page);
		current_loaded_page='30max';
		$('.signed-in-footer').html('<div class="footer-detail">\
										<div class="footer-value is_in_round">Not in Round</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Items:</div>\
										<div class="footer-value in_round_items">0</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Deposited:</div>\
										<div class="footer-value in_round_value">$0</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Chance:</div>\
										<div class="footer-value in_round_chance">0%</div>\
									</div>')
		if(current_appid==570)
		{
			current_potid=1;
			socket.emit('switch_pots', {potid: 1});
		}
		if(current_appid==433850)
		{
			current_potid=3;
			socket.emit('switch_pots', {potid: 3});
		}
		
	}
	if(page=='highroller')
	{
		show_notification('info','This gamemode is disabled for now.','30MAX will be returning later.',5000);
		return;
		clearInterval(intervalTimer);
		jackpot_timer=false;
		jackpot_animation=false;
		current_loaded_page='hr';
		//alert(page);
		$('.signed-in-footer').html('<div class="footer-detail">\
										<div class="footer-value is_in_round">Not in Round</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Items:</div>\
										<div class="footer-value in_round_items">0</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Deposited:</div>\
										<div class="footer-value in_round_value">$0</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Chance:</div>\
										<div class="footer-value in_round_chance">0%</div>\
									</div>')
		if(current_appid==570)
		{
			current_potid=2;
			socket.emit('switch_pots', {potid: 2});
		}
		if(current_appid==433850)
		{
			current_potid=4;
			socket.emit('switch_pots', {potid: 4});
		}
	}
	
	switch_page_top(current_loaded_page);
	
});

function switch_page_top(page)
{
	var apppage='dota2';
	var subpage='hr'
	if(current_appid==433850)
	{
		apppage='h1z1'
	}
	if(page!='coinflip')
	{
		if(current_potid==1)
		{
			subpage='hr'; // 30max later
		}
	}
	else
	{
		subpage='coinflip'
	}
	history.pushState(null, '', '../');
	history.pushState(null, '', '/'+apppage+'/'+subpage+'');
}

$(document).on("click", ".supply_drop", function(e)
{
	e.preventDefault();
	socket.emit('get_supply_drop', {user: user}, function(err, result)
	{
		if(err)
		{
			show_notification('warning','No Supply Drop Inbound!',result,5000);
		}
		else
		{
			 show_notification('info','Supply Drop Inbound',result,5000);

		}
	});
});

$(document).on("click", ".save_trade", function(e)
{
	e.preventDefault();
	var trade=$('.tradeurluser').val();
	socket.emit('trade_save', {user: user, trade: trade}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Unable to Save your Trade URL',err,5000);
		}
		else
		{
			 show_notification('success','Success!','Successfully saved your Trade URL',5000);

		}
	});
});

$(document).on("click", ".histRefresh", function(e)
{
	e.preventDefault();
	socket.emit('refresh_stats', {user:user}, function(err, result)
	{
	});
});

$(document).on("click", ".load_jackpot_data", function(e)
{
	e.preventDefault();
	stat_viewing='jackpot';
	$('.load_coinflip_data').removeClass('active');
	$('.load_trade_data').removeClass('active');
	$('.load_store_data').removeClass('active');
	$('.load_drop_data').removeClass('active');
	$(this).addClass('active');
	$('.myhistory-details').show();
	$('.total_deposited').html('$'+jackpot_amounts+' ('+jackpot_skins+' skins)');
	$('.total_bets').html(jackpot_bets);
	var profit_value=round(round(jackpot_won,2)-round(jackpot_amounts,2),2);
	var profit_class="";
	var profit_prefix="";
	if(profit_value==0) profit_class="levelprofit";
	if(profit_value>0)
	{
		profit_class="pos-profit";
		profit_prefix="+";
	}
	if(profit_value<0)
	{
		profit_value=profit_value*-1;
		profit_class="neg-profit";
		profit_prefix="-";
	}

	var style='\
			<div class="mh-label">Profit:</div>\
			<div class="mh-value '+profit_class+'">'+profit_prefix+'$'+profit_value+'</div>';
	//$('.total_profit').html(style);
});

var first_cf_click=false;
$(document).on("click", ".load_coinflip_data", function(e)
{
	e.preventDefault();
	stat_viewing='coinflip';
	$('.load_jackpot_data').removeClass('active');
	$('.load_trade_data').removeClass('active');
	$('.load_store_data').removeClass('active');
	$('.load_drop_data').removeClass('active');
	$(this).addClass('active');
	$('.myhistory-details').show();
	$('.total_deposited').html('$'+coinflip_amounts+' ('+coinflip_skins+' skins)');
	$('.total_bets').html(coinflip_bets);
	var profit_value=round(round(coinflip_won,2)-round(coinflip_amounts,2),2);
	var profit_class="";
	var profit_prefix="";
	if(profit_value==0) profit_class="levelprofit";
	if(profit_value>0)
	{
		profit_class="pos-profit";
		profit_prefix="+";
	}
	if(profit_value<0)
	{
		profit_value=profit_value*-1;
		profit_class="neg-profit";
		profit_prefix="-";
	}

	var style='\
			<div class="mh-label">Profit:</div>\
			<div class="mh-value '+profit_class+'">'+profit_prefix+'$'+profit_value+'</div>';
	//$('.total_profit').html(style);
	
	if(first_cf_click==false)
	{
		first_cf_click=true;
		getCFHistory(user);
	}
});

var first_trade_click=false;
$(document).on("click", ".load_trade_data", function(e)
{
	e.preventDefault();
	stat_viewing='trade';
	$('.load_coinflip_data').removeClass('active');
	$('.load_jackpot_data').removeClass('active');
	$('.load_store_data').removeClass('active');
	$('.load_drop_data').removeClass('active');
	$(this).addClass('active');
	$('.myhistory-details').hide();
	if(first_trade_click==false)
	{
		first_trade_click=true;
		getTradeHistory(user);
	}
});

var first_store_click=false;
$(document).on("click", ".load_store_data", function(e)
{
	e.preventDefault();
	stat_viewing='store';
	$('.load_coinflip_data').removeClass('active');
	$('.load_jackpot_data').removeClass('active');
	$('.load_trade_data').removeClass('active');
	$('.load_drop_data').removeClass('active');
	$(this).addClass('active');
	$('.myhistory-details').hide();
	if(first_store_click==false)
	{
		first_store_click=true;
		getStoreHistory(user);
	}
});

var first_drop_click=false;
$(document).on("click", ".load_drop_data", function(e)
{
	e.preventDefault();
	stat_viewing='drop';
	$('.load_coinflip_data').removeClass('active');
	$('.load_jackpot_data').removeClass('active');
	$('.load_trade_data').removeClass('active');
	$('.load_store_data').removeClass('active');
	$(this).addClass('active');
	$('.myhistory-details').hide();
	if(first_drop_click==false)
	{
		first_drop_click=true;
		getDropHistory(user);
	}
});



var jackpot_animation=false;
socket.on('end_jackpot', function(data)
{
	if(jackpot_animation==true) return;
	jackpot_animation=true;
	console.log('end_jackpot')
	console.log(data);
	startJackpotRound(data.anim);	
	//startJackpotRound(data.winner, data.players);	
});

socket.on('show_deposits', function(data)
{
	if(jackpot_animation==true) return;
	resetJackpotCircle();
	console.log('show_deposits')
	console.log(data);
	// change player amount
    if(data.player_num > 0){
        $('.jackpot-waiting').remove();
        insertJackpotUsers(data.players);
    }
	
	
});

socket.on('show_deposit', function(data)
{
	if(jackpot_animation==true) return;
	console.log('show_deposit')
	console.log(data);
	$('.jackpot-waiting').remove();
	insertJackpotUser(data.player);
	if(data.player.steamid==my_steamid)
	{
		$('.is_in_round').html('In Round')
		$('.in_round_items').html(data.player.skin_amount)
		$('.in_round_value').html('$'+round(data.player.total_value,2))
		$('.in_round_chance').html(data.player.chances+'%')
	}
});


socket.on('send_skins', function(data)
{
	if(jackpot_animation==true) return;
	console.log('send_skins')
	console.log(data);
		//$('.jp-round-skins').html('');
	var style='';
	var loaded=0;
	data.skins.forEach(function(skin)
	{
		loaded++;
		if(loaded<20)
		{
			style='<div class="jp-round-item" style="border-bottom-color: '+skin.user_color+'">\
					<img src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="Round skin" class="jpri-img">\
				   \
					<div class="jpri-details">\
						<div class="jpri-name">'+skin.skin+'</div>\
						<div class="jpri-value">$'+skin.price+'</div>\
					</div>\
					<img class="jpri-userava" alt="User" src="'+skin.useravatar+'">\
				</div>';
			$('.jp-round-skins').prepend(style);
		}
	})
	$('.current-skins').html(data.overall);
});

socket.on('show_skins', function(data)
{
	if(jackpot_animation==true) return;
	console.log('show_skins')
	console.log(data);
	$('.jp-round-skins').html('');
	var style='';
	data.skins.forEach(function(skin)
	{
		style='<div class="jp-round-item" style="border-bottom-color: '+skin.user_color+'">\
                <img src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="Round skin" class="jpri-img">\
               \
                <div class="jpri-details">\
                    <div class="jpri-name">'+skin.skin+'</div>\
                    <div class="jpri-value">$'+skin.price+'</div>\
                </div>\
                <img class="jpri-userava" alt="User" src="'+skin.useravatar+'">\
            </div>';
		$('.jp-round-skins').prepend(style);
	})
	$('.current-skins').html(data.skins.length);
});

var is_admin=0;

/*
JP
*/

var jackpot_skins_selected=[];
var jackpot_skins=0;
var jackpot_skins_value=0;


$(document).on("click", ".jackpot_skin", function(e)
{
	if(jackpot_skins_selected.length==20 && $(this).hasClass("active")==false)
	{
		alert('Max 20')
		return;
	}
	toggleJPSkin($(this).data('assetid'));
});


function toggleJPSkin(assetid)
{
	var skin=$(`.jackpot_skin[data-assetid=${assetid}]`);
	skin.toggleClass('active');
	skin_name=skin.data('name');
	skin_assetid=skin.data('assetid');
	skin_appid=skin.data('appid');
	skin_image=skin.data('image');
	skin_value=parseFloat(skin.data('price'));
	skin_value=round(skin_value,2);
	if(skin.hasClass("active"))
	{
		jackpot_skins_selected.push({name: skin_name, assetid: skin_assetid , appid: skin_appid, image: skin_image});
		jackpot_skins_value+=skin_value;
		jackpot_skins_value=round(jackpot_skins_value,2);
		jackpot_skins++;
	}
	else
	{
		remove_jp_skin(skin_assetid);
		jackpot_skins_value-=skin_value;
		jackpot_skins_value=round(jackpot_skins_value,2);
		jackpot_skins--;
		if(jackpot_skins==0)
		{
			jackpot_skins_value=0;
		}
	}

	$(".depost-amount-selc").html('$'+jackpot_skins_value);
	$(".jackpot_deposit_btn_text").html('Deposit '+jackpot_skins+' items');
	$(".jpd-selected-value").html('$'+jackpot_skins_value+'');
	$(".jpd-selected-items").html('('+jackpot_skins+'/20)');

}

$(document).on("click", ".jackpot_deposit_btn_text", function(e)
{
	if(user)
	{
		if(jackpot_skins_selected.length>0)
		{
			show_notification('info','Request being sent','Your Deposit request is being forwarded',5000);
			socket.emit('jackpot_deposit', {user:user, skins: jackpot_skins_selected}, function(err, result)
			{
				if(err)
				{
					show_notification('error','Unable to Deposit',err,5000);
					return;
				}
				startTradeOffer('Jackpot Deposit',result.offerid,result.skins,result.hash);
			});
		}
		else
		{
			
		}
	}
});

function startTradeOffer(title, offerid, skins, hash)
{
	$('.in-trade-offer-items').html('');
	$('.o2fa-title').html(title);
	var style='<input value="'+hash[0]+'" type="tel" name="offercode-1" maxlength="1" pattern="[\d]*" tabindex="1" placeholder="·" autocomplete="off" disabled>\
			  <input value="'+hash[1]+'" type="tel" name="offercode-2" maxlength="1" pattern="[\d]*" tabindex="2" placeholder="·" autocomplete="off" disabled>\
			  <input value="'+hash[2]+'" type="tel" name="offercode-3" maxlength="1" pattern="[\d]*" tabindex="3" placeholder="·" autocomplete="off" disabled>\
			  <input value="'+hash[3]+'" type="tel" name="offercode-4" maxlength="1" pattern="[\d]*" tabindex="4" placeholder="·" autocomplete="off" disabled>\
			  <input value="'+hash[4]+'" type="tel" name="offercode-5" maxlength="1" pattern="[\d]*" tabindex="5" placeholder="·" autocomplete="off" disabled>\
			  <input value="'+hash[5]+'" type="tel" name="offercode-6" maxlength="1" pattern="[\d]*" tabindex="6" placeholder="·" autocomplete="off" disabled>';
	$('.oc-enter-code').html(style)
	var total_value=0;
	skins.forEach(function(skin)
	{
		total_value+=round(skin.price,2);
		var skin='<div class="in-trade-offer-item">\
                 <div class="itoi-img-cont">\
                      <img class="itoi-img" alt="User item" src="'+skin.image+'">\
                 </div>\
                 <div class="itoi-txthold">\
                     <div class="itoi-name">'+skin.name+'</div>\
                     <div class="itoi-value">$'+skin.price+'</div> \
                 </div>\
              </div>'
		$('.in-trade-offer-items').append(skin);
	})
	$('.o2fa-value').html(total_value);
	var style='<a class="o2fa-btn viewOnSteamBtn" href="https://steamcommunity.com/tradeoffer/'+offerid+'" target="_offer'+offerid+'" style="display: block;"><i class="fab fa-steam"></i> View on steam</a>\
            <div class="o2fa-btn closeBtn">Dismiss</div>';
	$('.o2fa-btns-1').html(style);
	$('#offer2fa').modal('show');
    $('.sending-trade-offer').css('height', '0px');
    setTimeout(function(){
        $('.sent-trade-offer-txt').css('height', '115px');
        $('.enter-o2fa-code').css('height', '110px');
    }, 500);
}


$(document).on("click", ".viewOnSteamBtn", function(e)
{
	$('#jackpotDeposit').modal('hide');
	$('#coinflipDeposit').modal('hide');
	$('#offer2fa').modal('hide');
});

$(document).on("click", ".closeBtn", function(e)
{
	$('#jackpotDeposit').modal('hide');
	$('#coinflipDeposit').modal('hide');
	$('#offer2fa').modal('hide');
});

$(document).on("click", ".pd-btn.refresh", function(e)
{
	alert('scream')
});

var jp_skins_loaded=[];

$(document).on("click", ".deposit-btn", function(e)
{
	jackpot_skins_selected=[];
	jackpot_skins=0;
	jackpot_skins_value=0;
	
	$('.jackpot-deposit-cont').html('')
	$(".depost-amount-selc").html('$'+jackpot_skins_value);
	$(".jackpot_deposit_btn_text").html('Deposit '+jackpot_skins+' items');
	$(".jpd-selected-value").html('$'+jackpot_skins_value+'');
	$(".jpd-selected-items").html('('+jackpot_skins+'/20)');
	$('.jackpot_deposit_total_text').html('0 Total items ($0)')

	if(user)
	{
		socket.emit('load_inventory', {user:user}, function(err, result)
		{
			if(err)
			{
				show_notification('error','Inventory loading error',err,5000);
				return;
			}
			jp_skins_loaded=[];
			var total_value=0;
			result.forEach(function(skin)
			{
				jp_skins_loaded.push(skin);
				var style='<div class="deposit-skin-item jackpot_skin" onClick="" data-image="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" data-name="'+skin.market_hash_name+'" data-assetid="'+skin.assetid+'" data-appid="'+skin.appid+'" data-price="'+skin.price+'">\
								<img class="dsi-img" src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="'+skin.market_hash_name+'">\
								<div class="dsi-price">$'+skin.price+'</div>\
								<div class="dsi-name">'+skin.market_hash_name+'</div>\
							</div>';
				$('.jackpot-deposit-cont').prepend(style);
				total_value+=skin.price;
			})
			total_value=round(total_value,2);
			$('.jackpot_deposit_total_text').html(''+result.length+' Total items ($'+total_value+')')
		});
	}
});

$(document).on("click", ".jp_refresh", function(e)
{
	jackpot_skins_selected=[];
	jackpot_skins=0;
	jackpot_skins_value=0;
	
	$('.jackpot-deposit-cont').html('')
	$(".depost-amount-selc").html('$'+jackpot_skins_value);
	$(".jackpot_deposit_btn_text").html('Deposit '+jackpot_skins+' items');
	$(".jpd-selected-value").html('$'+jackpot_skins_value+'');
	$(".jpd-selected-items").html('('+jackpot_skins+'/20)');
	$('.jackpot_deposit_total_text').html('0 Total items ($0)')

	if(user)
	{
		socket.emit('load_inventory', {user:user}, function(err, result)
		{
			if(err)
			{
				show_notification('error','Inventory loading error',err,5000);
				return;
			}
			jp_skins_loaded=[];
			var total_value=0;
			result.forEach(function(skin)
			{
				var style='<div class="deposit-skin-item jackpot_skin" onClick="" data-image="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" data-name="'+skin.market_hash_name+'" data-assetid="'+skin.assetid+'" data-appid="'+skin.appid+'" data-price="'+skin.price+'">\
								<img class="dsi-img" src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="'+skin.market_hash_name+'">\
								<div class="dsi-price">$'+skin.price+'</div>\
								<div class="dsi-name">'+skin.market_hash_name+'</div>\
							</div>';
				$('.jackpot-deposit-cont').prepend(style);
				total_value+=skin.price;
			})
			total_value=round(total_value,2);
			$('.jackpot_deposit_total_text').html(''+result.length+' Total items ($'+total_value+')')
		});
	}
});

$(document).on("click", ".jp_empty", function(e)
{
	jackpot_skins_selected=[];
	jackpot_skins=0;
	jackpot_skins_value=0;
	
	$('.jackpot-deposit-cont').html('')
	$(".depost-amount-selc").html('$'+jackpot_skins_value);
	$(".jackpot_deposit_btn_text").html('Deposit '+jackpot_skins+' items');
	$(".jpd-selected-value").html('$'+jackpot_skins_value+'');
	$(".jpd-selected-items").html('('+jackpot_skins+'/20)');
	$('.jackpot_deposit_total_text').html('0 Total items ($0)')
	
	var total_value=0;
	if(jp_skins_loaded.length>0)
	{

		jp_skins_loaded.forEach(function(skin)
		{
			jp_skins_loaded.push(skin);
			var style='<div class="deposit-skin-item jackpot_skin" onClick="" data-image="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" data-name="'+skin.market_hash_name+'" data-assetid="'+skin.assetid+'" data-appid="'+skin.appid+'" data-price="'+skin.price+'">\
							<img class="dsi-img" src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="'+skin.market_hash_name+'">\
							<div class="dsi-price">$'+skin.price+'</div>\
							<div class="dsi-name">'+skin.market_hash_name+'</div>\
						</div>';
			$('.jackpot-deposit-cont').prepend(style);
			total_value+=skin.price;
		})
		total_value=round(total_value,2);
		$('.jackpot_deposit_total_text').html(''+jp_skins_loaded.length+' Total items ($'+total_value+')')
	}
});

/*
jp
*/

var current_captcha='';

socket.on('system_message', function(data)
{
	send_system_message(data.message);
});

socket.on('start_drop', function(data)
{
	$('.supply-drop-cont').show();
	send_system_message(data.message);
});

socket.on('end_drop', function(data)
{
	console.log('end_drop');
	console.log(data)
	$('.supply-p-1').hide();
	$('.supply-p-2').show();
	startSupplyDropAnimation(data.avatars,data.winnername,data.winnerid,data.message);
});

function send_system_message(message)
{
	var style='<div class="chat-msg message_system chatuid_system" data-messageid="-1" data-userid="-1" data-avatar="-1" data-name="system">\
					<div class="chat-avatar-cont">\
						<img src="/assets/img/system.png" class="chat-avatar">\
					</div>\
					<div class="chat-msg-cont">\
						<span class="chat-rank developer">System:</span><span class="chat-name"></span><span class="chat-msg-txt">'+message+'</span>\
					</div>\
				</div>';

  $('.chat-msgs-cont').prepend(style);
  if($('.chat-msgs-cont').scrollTop() + $('.chat-msgs-cont').innerHeight() >= $('.chat-msgs-cont')[0].scrollHeight) {
		return;
	} else {
		$('.chat-msgs-cont').scrollTop($('.chat-msgs-cont')[0].scrollHeight);
	}
}

socket.on('update_captcha', function(data)
{
	$('.bot-code-input').val('');
	current_captcha=data.result;
	$('.supply-p-1').show();
	$('.drop-captcha').html(current_captcha);
});

$(document).on("click", ".bot-code-btn", function(e)
{
	var captcha=$('.bot-code-input').val();
	socket.emit('join_drop', {user:user, captcha: captcha}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Unable to Enter',err,5000);
			return;
		}
		send_system_message(result);
	});
});

function startSupplyDropAnimation(avatars,winnername,winnerid,message)
{
	var msg_sent=false;
        var spinnerCode = '',
            supplyCounter = 0,
            supplyCounter2 = 0,
            userData = [];
		// Insert the users avatars
		var i=0;
		avatars.forEach(function(avatar,index)
		{
			spinnerCode+=`
                <div id="sau-` + i + `" class="supply-animation-user" style="opacity: 0;">
                    <img src="`+avatar+`" alt="avatar" class="sau-img"/>
                </div>
            `;
			if(winnerid!=index)
			{
				userData.push(i);
			}
			i++;
		})
        
        // Start inserting the users
        
        $('.supplydrop-animationarea').html(spinnerCode);
        
        var supplyDropInsert = setInterval(function(){
            $('#sau-' + supplyCounter).css('opacity', '1');
            supplyCounter++;
            if(supplyCounter == i){
                // Animation start
                var supplyDropRemove = setInterval(function(){
                    var removethis = userData[Math.floor(Math.random()*userData.length)],
                        removeThisToo = userData.findIndex(x => x == removethis);
                    
                        // This is the winner
                        if(userData.length == 0){
                           // This is end of the animation
                            $('#sau-' + winnerid).addClass('winner');
                            $('.supplydrop-winnertxt').html(''+removeLinks(removeXSS(winnername))+' wins !');
                            $('.supplydrop-winnertxt').css('opacity', '1');
                            $('.supplydrop-winnertxt').css('width', '200px');
                           
						   setTimeout(function()
						   {
							   if(msg_sent==true) return;
							   msg_sent=true;
							   send_system_message(message);
						   },2500);
						   setTimeout(function()
						   {
							   $('.supply-p-2').hide();
							   setTimeout(function()
							   {
								   $('.supplydrop-winnertxt').html('');
								   $('.supply-drop-cont').hide();
							   },3000);
						   },6000);
						   
                            setInterval(function(){
                                $('.supplydrop-winnertxt').css('color', '#fff');
                                clearInterval(supplyDropRemove);
                           }, 500);
                        } else {
                       // This is NOT the winner
                        $('#sau-' + removethis).css('opacity', '.1');
                        userData.splice(removeThisToo,1);
                    }
                    
                    supplyCounter2++;
                }, 450);
                clearInterval(supplyDropInsert);
            }
       }, 250);
    }
/*
cf
*/


var coinflip_skins_selected=[];
var coinflip_skins=0;
var coinflip_skins_value=0;
var coinflip_pick=1;
var coinflip_games={};
var coinflip_history={};
var cf_skins_loaded=[];
var cf_type='host';
var current_cf=-1;
var current_cf_min=0;
var current_cf_max=10000000;

coinflip_games[570]={};
coinflip_games[433850]={};

coinflip_history[570]={};
coinflip_history[433850]={};


$(document).on('hide.bs.modal','#coinflipLobby', function () {
	current_cf=-1;
	current_cf_min=0;
	current_cf_max=10000000;
});

socket.on('is_admin', function(data)
{
	is_admin=data;
	if(is_admin>2)
	{
		$('.new_supp').show();
	}
	if(is_admin>1)
	{
		$('.adminPanelToggle').show();
	}
});

socket.on('remove_flip', function(data)
{
	$('.cf_lobby_'+data.gameid+'').remove();
	delete coinflip_games[data.appid][data.gameid];
});

socket.on('end_flip', function(data)
{
	console.log('end_flip');
	console.log(data);
	coinflip_games[data.appid][data.gameid].winnerid=data.winnerid;
	coinflip_games[data.appid][data.gameid].winneravatar=data.winneravatar;
	coinflip_games[data.appid][data.gameid].winnername=data.winnername;
	coinflip_games[data.appid][data.gameid].server_seed=data.server_seed;
	coinflip_games[data.appid][data.gameid].random_seed=data.random_seed;
	coinflip_games[data.appid][data.gameid].random=data.random;
	coinflip_games[data.appid][data.gameid].signature=data.signature;
	coinflip_games[data.appid][data.gameid].serialid=data.serialid;
	coinflip_games[data.appid][data.gameid].module=data.module;
	coinflip_games[data.appid][data.gameid].winnerflip=data.winnerflip;
	coinflip_games[data.appid][data.gameid].removing=1;
	var game=coinflip_games[data.appid][data.gameid];
	if(current_cf==data.gameid)
	{		
		startCfAnimation(data.winnerflip);
		setTimeout(function()
		{			
			var loserid=game.hostid;
			if(data.winnerid==game.hostid) loserid=game.partid;

			$('.cf_game_'+data.gameid+'_user_'+loserid+'').css("opacity", "0.3");

			var winner_coin='red';
			if(data.winnerflip==1) winner_coin='blue';
			
			setTimeout(function()
			{
				$('.cf_footer').html('<div class="cf-winning-roll">\
							<span class="cf-wr-title">Module (%)</span>\
							  <span class="cf-wr-data">'+data.module+'</span><br>\
							  <span class="cf-wr-title">Hash</span>\
							  <span class="cf-wr-data">'+coinflip_games[data.appid][data.gameid].hash+'</span><br>\
							  <span class="cf-wr-title">Server seed</span>\
							  <span class="cf-wr-data">'+data.server_seed+'</span><br>\
							  <span class="cf-wr-title">Random seed</span>\
							  <span class="cf-wr-data">'+data.random_seed+'</span><br>\
							  <span class="cf-wr-title">Random.org Data</span><br>\
							  <span class="cf-wr-data">'+data.random+'</span><br>\
							  <span class="cf-wr-title">Random.org Signature</span><br>\
							  <span class="cf-wr-data">'+data.signature+'<br></span>\
							  <span class="cf-wr-title">Random.org Serial ID</span>\
							  <span class="cf-wr-data">'+data.serialid+'</span>');
							  
			},3000);			
		},6000);
	}
	setTimeout(function()
	{
		var winner_coin='red';
		if(data.winnerflip==1) winner_coin='blue';		
		$('.cf_price_'+data.gameid+'').html('<div class="cago-winner-disp">\
			<img src="'+data.winneravatar+'" alt="'+removeLinks(removeXSS(data.winnername))+'" title="'+removeLinks(removeXSS(data.winnername))+'" class="cagi-user-img">\
			<div class="cagi-coin cagic-'+winner_coin+'"></div>\
		</div>');
		
		setTimeout(function()
		{
			$('.cf_lobby_'+data.gameid+'').remove();
			show_lobby(coinflip_games[data.appid][data.gameid]);
			setTimeout(function()
			{
				$('.cf_lobby_'+data.gameid+'').remove();	
				delete coinflip_games[data.appid][data.gameid]; // delete push and move to history
				update_coinflip_stats(current_appid);
			},900000);
		},5000);
			
	},10000);
	
	/*setTimeout(function()
	{
		$('.cf_lobby_'+data.gameid+'').remove();
		show_lobby(coinflip_games[data.appid][data.gameid]);
		setTimeout(function()
		{
			$('.cf_lobby_'+data.gameid+'').remove();	
			delete coinflip_games[data.appid][data.gameid]; // delete push and move to history
		},900000);
	},1200000);
	*/
	update_coinflip_stats(current_appid);
});

socket.on('cancel_joining', function(data)
{
	console.log('cancel_joining');
	console.log(data);
	coinflip_games[data.appid][data.gameid].status='open';
	coinflip_games[data.appid][data.gameid].partid='';
	coinflip_games[data.appid][data.gameid].partname='';
	coinflip_games[data.appid][data.gameid].partavatar='';
	$('.cf_part_'+data.gameid).html('<div class="cagiu-empty">\
										<i class="far fa-plus"></i>\
									</div>');
	$('.cf_join_btn_'+data.gameid).show();
	if(current_cf==data.gameid)
	{
		view_lobby(coinflip_games[data.appid][data.gameid]);
	}
	update_coinflip_stats(current_appid);
});

socket.on('finish_joining', function(data)
{
	console.log('finish_joining');
	console.log(data);
	coinflip_games[data.appid][data.gameid].status='joined';
	coinflip_games[data.appid][data.gameid].partid=data.userid;
	coinflip_games[data.appid][data.gameid].partname=data.name;
	coinflip_games[data.appid][data.gameid].partavatar=data.avatar;
	coinflip_games[data.appid][data.gameid].partvalue=data.value;
	coinflip_games[data.appid][data.gameid].part_skins=data.skins;
	$('.cf_part_'+data.gameid).html('<img src="'+data.avatar+'" alt="'+removeLinks(removeXSS(data.name))+'" title="'+removeLinks(removeXSS(data.name))+'" class="cagi-user-img">');
	$('.cf_join_btn_'+data.gameid).hide();
	if(current_cf==data.gameid)
	{
		view_lobby(coinflip_games[data.appid][data.gameid]);
	}
});

socket.on('add_joining', function(data)
{
	console.log('add_joining');
	console.log(data);
	coinflip_games[data.appid][data.gameid].status='joining';
	coinflip_games[data.appid][data.gameid].partid=data.userid;
	coinflip_games[data.appid][data.gameid].partname=data.name;
	coinflip_games[data.appid][data.gameid].partavatar=data.avatar;
	$('.cf_part_'+data.gameid).html('<div class="circle-loader join_'+data.gameid+'"></div><img style="opacity: 0.4;" src="'+data.avatar+'" alt="'+removeLinks(removeXSS(data.name))+'" title="'+removeLinks(removeXSS(data.name))+'" class="cagi-user-img">');
	$('.cf_join_btn_'+data.gameid).hide();
	if(current_cf==data.gameid)
	{
		view_lobby(coinflip_games[data.appid][data.gameid]);
	}
});

socket.on('update_cf_stats', function(data)
{
	/*
	$('.cf_total_value').html(round(data.coinflip_value,2));
	$('.cf_total_items').html(data.total_items);
	if(data.joinable_games<0)
	{
		data.joinable_games=0;
	}
	$('.cf_total_joinable').html(data.joinable_games);
	*/
});

socket.on('cf_bot_ban', function(data)
{
	for(var key in data.lobbies[570])
	{
		var lobby=data.lobbies[570][key];
		$('.cf_lobby_'+lobby.gameid+'').remove();	
	}
	for(var key in data.lobbies[443850])
	{
		var lobby=data.lobbies[443850][key];
		$('.cf_lobby_'+lobby.gameid+'').remove();	
	}
	coinflip_games[570]={};
	coinflip_games[433850]={};
});

socket.on('show_cf_lobbies', function(data)
{
	coinflip_games=data.lobbies;
	console.log('show_cf_lobbies');
	console.log(data);
	$('.coinflip_lobbies').html('');
	$('.coinflip_recent').html('');
	for(var key in data.lobbies[570])
	{
		var lobby=data.lobbies[570][key];
		if(lobby.winnerid)
		{
			setTimeout(function()
			{
				$('.cf_lobby_'+lobby.gameid+'').remove();	
				delete coinflip_games[lobby.appid][lobby.gameid]; // delete push and move to history
				update_coinflip_stats(current_appid);
			},900000);
		}
		if(current_appid==570)
		{
			show_lobby(lobby);		
		}
	}
	for(var key in data.lobbies[433850])
	{
		var lobby=data.lobbies[433850][key];
		if(lobby.winnerid)
		{
			setTimeout(function()
			{
				$('.cf_lobby_'+lobby.gameid+'').remove();	
				delete coinflip_games[lobby.appid][lobby.gameid]; // delete push and move to history
				update_coinflip_stats(current_appid);
			},900000);
		}
		if(current_appid==433850)
		{
			show_lobby(lobby);		
		}
	}
	update_coinflip_stats(current_appid);
});

socket.on('show_cf_lobby', function(data)
{
	coinflip_games[data.appid][data.gameid]=data.lobby
	console.log('show_cf_lobby');
	console.log(data);
	//$('.coinflip_lobbies').html('');
	show_lobby(data.lobby);		
	update_coinflip_stats(current_appid);
});

function show_lobby(lobby)
{
	var host_color="blue";
	var part_color="red";
	var part_side=2;
	if(lobby.hostchoice==2)
	{
		part_side=1;
		host_color="red";
		part_color="blue";
	}
	var part_avatar='<div class="cagiu-empty">\
							<i class="far fa-plus"></i>\
						</div>';
	
	var fade_1='';
	var fade_2='';
	if(lobby.status=='joining')
	{
		fade_1='<div class="circle-loader join_'+lobby.gameid+'"></div>';
		fade_2='style="opacity: 0.4;"';
	}
	
	if(lobby.partid)
	{
		part_avatar=''+fade_1+'<img '+fade_2+' src="'+lobby.partavatar+'" alt="'+removeLinks(removeXSS(lobby.partname))+'" title="'+removeLinks(removeXSS(lobby.partname))+'" class="cagi-user-img">';
	}
		
	var join_button='';
	if((my_steamid!=-1&&lobby.partid!=-1&&lobby.status=="open"&&lobby.hostid!=my_steamid)||is_admin>0)
	{
		join_button='<div class="cagi-btn cagi-join cf_join_btn_'+lobby.gameid+'" data-toggle="modal" data-target="#coinflipDeposit" data-side="'+part_side+'" data-gameid="'+lobby.gameid+'" data-value="'+lobby.hostvalue+'" data-min="'+lobby.min_val+'" data-max="'+lobby.max_val+'">Join</div>';
	}
		
	
	var skin_style='';
	var skins_loaded=0;
	var extra_skins=0;
	lobby.host_skins.forEach(function(skin)
	{
		if(skins_loaded>5)
		{
			extra_skins++;
		}
		else
		{
			skin_style+='<div class="cagi-item-cont">\
								<img src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="'+removeLinks(removeXSS(skin.skin))+' ($'+skin.price+')" title="'+removeLinks(removeXSS(skin.skin))+' ($'+skin.price+')" class="cagi-item">\
							</div>';
		}
		skins_loaded++;
	})
	
	lobby.part_skins.forEach(function(skin)
	{
		if(skins_loaded>5)
		{
			extra_skins++;
		}
		else
		{
			skin_style+='<div class="cagi-item-cont">\
								<img src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="'+removeLinks(removeXSS(skin.skin))+' ($'+skin.price+')" title="'+removeLinks(removeXSS(skin.skin))+' ($'+skin.price+')" class="cagi-item">\
							</div>';
		}
		skins_loaded++;
	})
	
	if(extra_skins>0)
	{
		skin_style+='<div class="cagi-item-more">+'+extra_skins+' Skins</div>';
	}
	
	var style='<div class="cag-item cag-open cf_lobby_'+lobby.gameid+'" data-gameid="'+lobby.gameid+'" data-value="'+lobby.hostvalue+'" data-min="'+lobby.min_val+'" data-max="'+lobby.max_val+'">\
					<div class="cagi-users">\
						<div class="cagi-user">\
							<img src="'+lobby.hostavatar+'" alt="'+removeLinks(removeXSS(lobby.hostname))+'" title="'+removeLinks(removeXSS(lobby.hostname))+'" class="cagi-user-img">\
							<div class="cagi-coin cagic-'+host_color+'"></div>\
						</div>\
						<div class="cagi-user">\
							<span class="cf_part_'+lobby.gameid+'">'+part_avatar+'</span>\
							<div class="cagi-coin cagic-'+part_color+'"></div>\
						</div>\
					</div>\
					<div class="cagi-items">\
						'+skin_style+'\
					</div>\
					<div class="cagi-price cf_price_'+lobby.gameid+'">\
						<div class="cagi-value">$'+lobby.hostvalue+'</div>\
						<div class="cagi-range">$'+lobby.min_val+' to $'+lobby.max_val+'</div>\
					</div>\
					<div class="cagi-actions">\
						'+join_button+'\
						<div class="cagi-btn cagi-view" data-toggle="modal" data-target="#coinflipLobby" data-gameid="'+lobby.gameid+'" data-value="'+lobby.hostvalue+'" data-min="'+lobby.min_val+'" data-max="'+lobby.max_val+'">View</div>\
					</div>\
				</div>'
	if(lobby.winnerid)
	{
		$('.coinflip_recent').prepend(style);
		setTimeout(function()
		{
			var winner_coin='red';
			if(lobby.winnerflip==1) winner_coin='blue';
			$('.cf_price_'+lobby.gameid+'').html('<div class="cagi-value">$'+(round(lobby.hostvalue+lobby.partvalue,2))+'</div><div class="cago-winner-disp">\
															<img src="'+lobby.winneravatar+'" alt="'+removeLinks(removeXSS(lobby.winnername))+'" title="'+removeLinks(removeXSS(lobby.winnername))+'" class="cagi-user-img">\
															<div class="cagi-coin cagic-'+winner_coin+'"></div>\
														</div>');
			$('.cf_join_btn_'+lobby.gameid).show();
		},1000);
		setTimeout(function()
		{
			$('.cf_lobby_'+lobby.gameid+'').remove();
			var winner_coin='red';
			if(lobby.winnerflip==1) winner_coin='blue';
			$('.coinflip_history').prepend(style);
			$('.cf_price_'+lobby.gameid+'').html('<div class="cagi-value">$'+(round(lobby.hostvalue+lobby.partvalue,2))+'</div<div class="cago-winner-disp">\
															<img src="'+lobby.winneravatar+'" alt="'+removeLinks(removeXSS(lobby.winnername))+'" title="'+removeLinks(removeXSS(lobby.winnername))+'" class="cagi-user-img">\
															<div class="cagi-coin cagic-'+winner_coin+'"></div>\
														</div>');
			$('.cf_join_btn_'+lobby.gameid).show();
		},1200000);
	}
	else
	{
		$('.coinflip_lobbies').prepend(style);
	}
	$('.coinflip_lobbies .cag-item').sort(function(a, b) {
		return $(b).data('max') - $(a).data('max');
	}).appendTo('.coinflip_lobbies');
	
	$('.coinflip_recent .cag-item').sort(function(a, b) {
		return $(b).data('max') - $(a).data('max');
	}).appendTo('.coinflip_recent');
}

function update_coinflip_stats(appid)
{
	var total_value=0;
	var total_items=0;
	var joinable_games=0;
	for(var key in coinflip_games[appid])
	{
		var lobby=coinflip_games[appid][key];
		total_value+=round(lobby.hostvalue+lobby.partvalue,2);
		total_items+=lobby.host_skins.length+lobby.part_skins.length;
		if(lobby.status=='open')
		{
			joinable_games++;
		}
	}
	
	$('.cf_total_value').html(round(total_value,2));
	$('.cf_total_items').html(total_items);
	if(joinable_games<0)
	{
		joinable_games=0;
	}
	$('.cf_total_joinable').html(joinable_games);
}


$(document).on("click", ".cagi-view", function(e)
{
	var gameid=$(this).data('gameid');
	$('.coinflip-lobby').html('');
	if(coinflip_games[current_appid][gameid])
	{
		view_lobby(coinflip_games[current_appid][gameid]);
	}
	else
	{
		show_notification('error','Lobby error','Lobby not found',5000);
	}
});

function view_lobby(lobby)
{
	
	current_cf=lobby.gameid;
	current_cf_min=lobby.min_val;
	current_cf_max=lobby.max_val;
	$('.cf_title').html('Coinflip #'+lobby.gameid+'');
	$('.cf_footer').html('<div class="cf-winning-roll">\
							<span class="cf-wr-title">Hash</span>\
						  <span class="cf-wr-data">'+lobby.hash+'</span>');
	var host_color="blue";
	var part_color="red";
	var part_side=2;
	
	if(lobby.hostchoice==2)
	{
		part_side=1;
		host_color="red";
		part_color="blue";
	}
	var fade_1='';
	var fade_2='';
	if(lobby.status=='joining')
	{
		fade_1='<div class="circle-loader join_'+lobby.gameid+'"></div>';
		fade_2='style="opacity: 0.4;"';
	}
	
	var part_avatar='<img class="cf-user-avatar-img" alt="User" src="https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/b5/b5bd56c1aa4644a474a2e4972be27ef9e82e517e_full.jpg">';
	
	if(lobby.partid)
	{
		part_avatar=''+fade_1+'<img '+fade_2+' class="cf-user-avatar-img" alt="'+removeLinks(removeXSS(lobby.partname))+'" title="'+removeLinks(removeXSS(lobby.partname))+'" src="'+lobby.partavatar+'">';
	}
	else
	{
		lobby.partname='?';
	}
	
	if(my_steamid==lobby.hostid&&!lobby.partid)
	{
		$('.cf_title').append('&emsp;&emsp;<font size="1" class="pointer cancel_flip" data-gameid="'+lobby.gameid+'" data-appid="'+lobby.appid+'">(Cancel Flip)</font>');
	}
	
	var join_button='';
	if((my_steamid!=-1&&lobby.partid!=-1&&lobby.status=="open"&&lobby.hostid!=my_steamid)||is_admin>0)
	{
		join_button='<div class="cagi-btn cagi-join cf_join_btn_'+lobby.gameid+'" data-toggle="modal" data-target="#coinflipDeposit" data-side="'+part_side+'" data-gameid="'+lobby.gameid+'" data-value="'+lobby.hostvalue+'" data-min="'+lobby.min_val+'" data-max="'+lobby.max_val+'">Join</div>';
	}
	
	var hostpercent=50;
	var partpercent=50;
	var hostfrom=0;
	var partfrom=50;
	var hostto=50;
	var partto=100;
	if(lobby.partvalue>0)
	{
		var total_value=round(lobby.hostvalue+lobby.partvalue,2);
		hostpercent = round(lobby.hostvalue * 100 / total_value,2);
		partpercent = round(lobby.partvalue * 100 / total_value,2);
	}
	hostfrom=0;
	hostto=hostfrom+hostpercent;
	partfrom=hostfrom+hostpercent;
	partto=100;
	
	if(lobby.hostchoice==2)
	{
		hostfrom=partpercent;
		hostto=100;
		partfrom=0;
		partto=partpercent;	
	}
	
	var host_skins='';
	var part_skins='';
	
	lobby.host_skins.forEach(function(skin)
	{
		host_skins+='<div class="cf-l-skin">\
						  <div class="cf-l-img-cont">\
							  <img src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="skin" class="cf-l-img">\
						  </div>\
						  <span class="cf-l-skinname">'+skin.skin+'</span>\
						  <span class="cf-l-value">$'+skin.price+'</span>\
					  </div>';
	})
	
	lobby.part_skins.forEach(function(skin)
	{
		part_skins+='<div class="cf-l-skin">\
						  <div class="cf-l-img-cont">\
							  <img src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="skin" class="cf-l-img">\
						  </div>\
						  <span class="cf-l-skinname">'+skin.skin+'</span>\
						  <span class="cf-l-value">$'+skin.price+'</span>\
					  </div>';
	})
	
	
	if(lobby.part_skins.length==0)
	{
		part_skins=join_button;
	}

	
	var style='<div class="coinflip-user cf-user-1 cf-user-blue">\
				  <div class="cf-user-details">\
					  <div class="cf-user-avatar cf_game_'+lobby.gameid+'_user_'+lobby.hostid+'">\
						   <img class="cf-user-avatar-img" alt="'+lobby.hostname+'" title="'+removeLinks(removeXSS(lobby.hostname))+'" src="'+lobby.hostavatar+'">\
						   <div class="cagi-coin cagic-'+host_color+'"></div>\
					  </div>\
					  <div class="cf-user-details-txt">\
						  <h4 class="cf-username-title">'+removeLinks(removeXSS(lobby.hostname))+'</h4>\
						  <h5 class="cf-gamedetails">$'+round(lobby.hostvalue,2)+' &ensp; | &ensp;  <b>'+hostpercent+'%</b> &ensp; | &ensp; '+hostfrom+'% - '+hostto+'%</h5>\
					  </div>\
				  </div>\
				  <div class="cf-user-skins">\
					  '+host_skins+'\
				  </div>\
			 </div>\
			  <div class="coinflip-user cf-user-2 cf-user-red">\
				  <div class="cf-user-details">\
					  <div class="cf-user-avatar cf_game_'+lobby.gameid+'_user_'+lobby.partid+'">\
						   '+part_avatar+'\
						   <div class="cagi-coin cagic-'+part_color+'"></div>\
					  </div>\
					  <div class="cf-user-details-txt">\
						  <h4 class="cf-username-title">'+removeLinks(removeXSS(lobby.partname))+'</h4>\
						  <h5 class="cf-gamedetails">$'+round(lobby.partvalue,2)+' &ensp; | &ensp; <b>'+partpercent+'%</b> &ensp; | &ensp; '+partfrom+'% - '+partto+'%</h5>\
					  </div>\
				  </div>\
				  <div class="cf-user-skins">\
					 '+part_skins+'\
				  </div>\
			</div>\
			 </div>\
          <div class="cf-coin-animation-cont">\
              <div class="cf-coin-lobbyanimation-coin" style="background-image: url(\'/assets/img/cf_blue_bait.png\');"></div>\
              <div class="cf-countdown-ani" style="display: none;"></div>\
          </div>';
			
			$('.coinflip-lobby').html(style);
	if(lobby.winnerid)
	{
		startCfAnimation(lobby.winnerflip);
		setTimeout(function()
		{
			
			var loserid=lobby.hostid;
			if(lobby.winnerid==lobby.hostid) loserid=lobby.partid;

			$('.cf_game_'+lobby.gameid+'_user_'+loserid+'').css("opacity", "0.3");

			var winner_coin='red';
			if(lobby.winnerflip==1) winner_coin='blue';
			
			setTimeout(function()
			{
				$('.cf_footer').html('<div class="cf-winning-roll">\
							<span class="cf-wr-title">Module (%)</span>\
							  <span class="cf-wr-data">'+lobby.module+'</span><br>\
							  <span class="cf-wr-title">Hash</span>\
							  <span class="cf-wr-data">'+lobby.hash+'</span><br>\
							  <span class="cf-wr-title">Server seed</span>\
							  <span class="cf-wr-data">'+lobby.server_seed+'</span><br>\
							  <span class="cf-wr-title">Random seed</span>\
							  <span class="cf-wr-data">'+lobby.random_seed+'</span><br>\
							  <span class="cf-wr-title">Random.org Data</span><br>\
							  <span class="cf-wr-data">'+lobby.random+'</span><br>\
							  <span class="cf-wr-title">Random.org Signature</span><br>\
							  <span class="cf-wr-data">'+lobby.signature+'<br></span>\
							  <span class="cf-wr-title">Random.org Serial ID</span>\
							  <span class="cf-wr-data">'+lobby.serialid+'</span>');
							  
			},3000);
		},6000);
	}
}


socket.on('cancel_flip', function(data)
{
	delete coinflip_games[data.appid][data.gameid];
	$('.cf_lobby_'+data.gameid+'').remove();
	if(current_cf==data.gameid)
	{
		show_notification('error','Flip Cancelled','The Coinflip you are viewing has been cancelled.',5000);
	}
	update_coinflip_stats(current_appid);
});

$(document).on("click", ".cancel_flip", function(e)
{
	var gameid=$(this).data("gameid");
	var appid=$(this).data("appid");
	var cancel = confirm("Are you sure you want to cancel this flip?");
	if (cancel == true)
	{
	  	socket.emit('cancel_lobby', {user:user, appid: appid, gameid: gameid}, function(err, result)
		{
			if(err)
			{
				show_notification('error','Unable to Cancel',err,5000);
				return;
			}
			
		});
	}
});

$(document).on("click", ".cf_host_blue", function(e)
{
	$('.cf_host_blue').addClass('active');
	$('.cf_host_red').removeClass('active');
	coinflip_pick=1;
});

$(document).on("click", ".cf_host_red", function(e)
{
	$('.cf_host_red').addClass('active');
	$('.cf_host_blue').removeClass('active');
	coinflip_pick=2;
});

$(document).on("click", ".coinflip_skin", function(e)
{
	if(coinflip_skins_selected.length==20 && $(this).hasClass("active")==false)
	{
		alert('Max 20')
		return;
	}
	toggleCFSkin($(this).data('assetid'));
});


function toggleCFSkin(assetid)
{
	var skin=$(`.coinflip_skin[data-assetid=${assetid}]`);
	skin.toggleClass('active');
	skin_name=skin.data('name');
	skin_assetid=skin.data('assetid');
	skin_appid=skin.data('appid');
	skin_image=skin.data('image');
	skin_value=parseFloat(skin.data('price'));
	skin_value=round(skin_value,2);
	if(skin.hasClass("active"))
	{
		coinflip_skins_selected.push({name: skin_name, assetid: skin_assetid , appid: skin_appid, image: skin_image, price: skin_value});
		coinflip_skins_value+=skin_value;
		coinflip_skins_value=round(coinflip_skins_value,2);
		coinflip_skins++;
	}
	else
	{
		remove_cf_skin(skin_assetid);
		coinflip_skins_value-=skin_value;
		coinflip_skins_value=round(coinflip_skins_value,2);
		coinflip_skins--;
		if(coinflip_skins==0)
		{
			coinflip_skins_value=0;
		}
	}

	$(".depost-amount-selc").html('$'+coinflip_skins_value);
	if(cf_type=='host')
	{
		$(".coinflip_deposit_btn_text").html('Host with '+coinflip_skins+' Skins');
	}
	else
	{
		$(".coinflip_deposit_btn_text").html('Join with '+coinflip_skins+' Skins');
	}
	$(".cfd-selected-value").html('$'+coinflip_skins_value+'');
	$(".cfd-selected-items").html('('+coinflip_skins+'/20)');

}

$(document).on("click", ".coinflip_deposit_btn_text", function(e)
{
	if(user)
	{
		if(coinflip_skins_selected.length>0)
		{
			if(cf_type=='host')
			{
				show_notification('info','Request being sent','Your Host request is being forwarded',5000);
				socket.emit('host_lobby', {user:user, skins: coinflip_skins_selected, side: coinflip_pick, appid: current_appid}, function(err, result)
				{
					if(err)
					{
						show_notification('error','Unable to Host',err,5000);
						return;
					}
					startTradeOffer('Coinflip Host',result.offerid,result.skins,result.hash);
				});
			}
			else
			{
				show_notification('info','Request being sent','Your Join request is being forwarded',5000);
				socket.emit('join_lobby', {gameid: current_cf, user:user, skins: coinflip_skins_selected, appid: current_appid}, function(err, result)
				{
					if(err)
					{
						show_notification('error','Unable to Join',err,5000);
						return;
					}
					startTradeOffer('Coinflip Joining',result.offerid,result.skins,result.hash);
				});
			}
		}
		else
		{
			
		}
	}
});

function startCFTradeOffer(title, offerid, skins, hash)
{
	$('.in-trade-offer-items').html('');
	$('.o2fa-title').html(title);
	var style='<input value="'+hash[0]+'" type="tel" name="offercode-1" maxlength="1" pattern="[\d]*" tabindex="1" placeholder="·" autocomplete="off" disabled>\
			  <input value="'+hash[1]+'" type="tel" name="offercode-2" maxlength="1" pattern="[\d]*" tabindex="2" placeholder="·" autocomplete="off" disabled>\
			  <input value="'+hash[2]+'" type="tel" name="offercode-3" maxlength="1" pattern="[\d]*" tabindex="3" placeholder="·" autocomplete="off" disabled>\
			  <input value="'+hash[3]+'" type="tel" name="offercode-4" maxlength="1" pattern="[\d]*" tabindex="4" placeholder="·" autocomplete="off" disabled>\
			  <input value="'+hash[4]+'" type="tel" name="offercode-5" maxlength="1" pattern="[\d]*" tabindex="5" placeholder="·" autocomplete="off" disabled>\
			  <input value="'+hash[5]+'" type="tel" name="offercode-6" maxlength="1" pattern="[\d]*" tabindex="6" placeholder="·" autocomplete="off" disabled>';
	$('.oc-enter-code').html(style)
	var total_value=0;
	skins.forEach(function(skin)
	{
		total_value+=round(skin.price,2);
		var skin='<div class="in-trade-offer-item">\
                 <div class="itoi-img-cont">\
                      <img class="itoi-img" alt="User item" src="'+skin.image+'">\
                 </div>\
                 <div class="itoi-txthold">\
                     <div class="itoi-name">'+skin.name+'</div>\
                     <div class="itoi-value">$'+skin.price+'</div> \
                 </div>\
              </div>'
		$('.in-trade-offer-items').append(skin);
	})
	$('.o2fa-value').html(total_value);
	var style='<a class="o2fa-btn viewOnSteamBtn" href="https://steamcommunity.com/tradeoffer/'+offerid+'" target="_offer'+offerid+'" style="display: block;"><i class="fab fa-steam"></i> View on steam</a>\
            <div class="o2fa-btn closeBtn">Dismiss</div>';
	$('.o2fa-btns-1').html(style);
	$('#offer2fa').modal('show');
    $('.sending-trade-offer').css('height', '0px');
    setTimeout(function(){
        $('.sent-trade-offer-txt').css('height', '115px');
        $('.enter-o2fa-code').css('height', '110px');
    }, 500);
}


$(document).on("click", ".closeBtn", function(e)
{
	$('#offer2fa').modal('hide');
});

$(document).on("click", ".pd-btn.refresh", function(e)
{
	alert('scream')
});


$(document).on("click", ".cagi-join", function(e)
{
	coinflip_skins_selected=[];
	coinflip_skins=0;
	coinflip_skins_value=0;
	cf_type='join';

	var side=$(this).data('side');
	var gameid=$(this).data('gameid');
	var min_val=$(this).data('min');
	var max_val=$(this).data('max');
	
	current_cf=gameid;
	current_cf_min=min_val;
	current_cf_max=max_val;
	
	var part_side='<div class="cagic-blue cf-dep-coin cf_host_red active"></div>';
	if(side==2) part_side='<div class="cagic-blue cf-dep-coin active"></div>';
	$('.cf-sides-cont').html('<div class="cf-sides-labels">Your Side:</div>\
                '+part_side+'')
	$('.cf_offer_title').html('Join Lobby #'+gameid+' &emsp; | &emsp; $'+min_val+' - $'+max_val+'')
	$('.coinflip-deposit-cont').html('')
	$(".depost-amount-selc").html('$'+coinflip_skins_value);
	$(".coinflip_deposit_btn_text").html('Join with '+coinflip_skins+' Skins');
	$(".cfd-selected-value").html('$'+coinflip_skins_value+'');
	$(".cfd-selected-items").html('('+coinflip_skins+'/20)');
	$('.coinflip_deposit_total_text').html('0 Total items ($0)')

	if(user)
	{
		socket.emit('load_cf_inventory', {user:user, appid: current_appid}, function(err, result)
		{
			if(err)
			{
				show_notification('error','Inventory loading error',err,5000);
				return;
			}
			cf_skins_loaded=[];
			var total_value=0;
			result.forEach(function(skin)
			{
				if(skin.price<=max_val)
				{
					cf_skins_loaded.push(skin);
					var style='<div class="deposit-skin-item coinflip_skin" onClick="" data-image="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" data-name="'+skin.market_hash_name+'" data-assetid="'+skin.assetid+'" data-appid="'+skin.appid+'" data-price="'+skin.price+'">\
									<img class="dsi-img" src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="'+skin.market_hash_name+'">\
									<div class="dsi-price">$'+skin.price+'</div>\
									<div class="dsi-name">'+skin.market_hash_name+'</div>\
								</div>';
					$('.coinflip-deposit-cont').prepend(style);
					total_value+=skin.price;
				}
			})
			total_value=round(total_value,2);
			$('.coinflip_deposit_total_text').html(''+result.length+' Total items ($'+total_value+')')
		});
	}
});

$(document).on("click", ".create-game-btn", function(e)
{
	coinflip_skins_selected=[];
	coinflip_skins=0;
	coinflip_skins_value=0;
	coinflip_pick=1;
	cf_type='host';
	
	$('.cf-sides-cont').html('<div class="cf-sides-labels">Choose a Side:</div>\
                <div class="cagic-blue cf-dep-coin cf_host_blue active"></div>\
                <div class="cagic-red cf-dep-coin cf_host_red"></div>')
	$('.cf_offer_title').html('Host Lobby')
	$('.coinflip-deposit-cont').html('')
	$(".depost-amount-selc").html('$'+coinflip_skins_value);
	$(".coinflip_deposit_btn_text").html('Host with '+coinflip_skins+' Skins');
	$(".cfd-selected-value").html('$'+coinflip_skins_value+'');
	$(".cfd-selected-items").html('('+coinflip_skins+'/20)');
	$('.coinflip_deposit_total_text').html('0 Total items ($0)')

	if(user)
	{
		socket.emit('load_cf_inventory', {user:user, appid: current_appid}, function(err, result)
		{
			if(err)
			{
				show_notification('error','Inventory loading error',err,5000);
				return;
			}
			cf_skins_loaded=[];
			var total_value=0;
			result.forEach(function(skin)
			{
				cf_skins_loaded.push(skin);
				var style='<div class="deposit-skin-item coinflip_skin" onClick="" data-image="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" data-name="'+skin.market_hash_name+'" data-assetid="'+skin.assetid+'" data-appid="'+skin.appid+'" data-price="'+skin.price+'">\
								<img class="dsi-img" src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="'+skin.market_hash_name+'">\
								<div class="dsi-price">$'+skin.price+'</div>\
								<div class="dsi-name">'+skin.market_hash_name+'</div>\
							</div>';
				$('.coinflip-deposit-cont').prepend(style);
				total_value+=skin.price;
			})
			total_value=round(total_value,2);
			$('.coinflip_deposit_total_text').html(''+result.length+' Total items ($'+total_value+')')
		});
	}
});

$(document).on("click", ".cf_refresh", function(e)
{
	coinflip_skins_selected=[];
	coinflip_skins=0;
	coinflip_skins_value=0;
	
	$('.coinflip-deposit-cont').html('')
	$(".depost-amount-selc").html('$'+coinflip_skins_value);
	if(cf_type=="host")
	{
		$(".coinflip_deposit_btn_text").html('Host with '+coinflip_skins+' skins');
	}
	else
	{
		$(".coinflip_deposit_btn_text").html('Join with '+coinflip_skins+' skins');
	}
	$(".cfd-selected-value").html('$'+coinflip_skins_value+'');
	$(".cfd-selected-items").html('('+coinflip_skins+'/20)');
	$('.coinflip_deposit_total_text').html('0 Total items ($0)')

	if(user)
	{
		socket.emit('load_cf_inventory', {user:user, appid: current_appid}, function(err, result)
		{
			if(err)
			{
				show_notification('error','Inventory loading error',err,5000);
				return;
			}
			cf_skins_loaded=[];
			var total_value=0;
			result.forEach(function(skin)
			{
				if(cf_type=='host')
				{
					cf_skins_loaded.push(skin);
					var style='<div class="deposit-skin-item coinflip_skin" onClick="" data-image="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" data-name="'+skin.market_hash_name+'" data-assetid="'+skin.assetid+'" data-appid="'+skin.appid+'" data-price="'+skin.price+'">\
									<img class="dsi-img" src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'" alt="'+skin.market_hash_name+'">\
									<div class="dsi-price">$'+skin.price+'</div>\
									<div class="dsi-name">'+skin.market_hash_name+'</div>\
								</div>';
					$('.coinflip-deposit-cont').prepend(style);
					total_value+=skin.price;
				}
				else
				{
					if(skin.price<=current_cf_max)
					{
						cf_skins_loaded.push(skin);
						var style='<div class="deposit-skin-item coinflip_skin" onClick="" data-image="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" data-name="'+skin.market_hash_name+'" data-assetid="'+skin.assetid+'" data-appid="'+skin.appid+'" data-price="'+skin.price+'">\
										<img class="dsi-img" src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="'+skin.market_hash_name+'">\
										<div class="dsi-price">$'+skin.price+'</div>\
										<div class="dsi-name">'+skin.market_hash_name+'</div>\
									</div>';
						$('.coinflip-deposit-cont').prepend(style);
						total_value+=skin.price;
					}
				}
			})
			total_value=round(total_value,2);
			$('.coinflip_deposit_total_text').html(''+result.length+' Total items ($'+total_value+')')
		});
	}
});

$(document).on("click", ".cf_empty", function(e)
{
	coinflip_skins_selected=[];
	coinflip_skins=0;
	coinflip_skins_value=0;
	
	$('.coinflip-deposit-cont').html('')
	$(".depost-amount-selc").html('$'+coinflip_skins_value);
	if(cf_type=="host")
	{
		$(".coinflip_deposit_btn_text").html('Host with '+coinflip_skins+' skins');
	}
	else
	{
		$(".coinflip_deposit_btn_text").html('Join with '+coinflip_skins+' skins');
	}
	$(".cfd-selected-value").html('$'+coinflip_skins_value+'');
	$(".cfd-selected-items").html('('+coinflip_skins+'/20)');
	$('.coinflip_deposit_total_text').html('0 Total items ($0)')
	
	var total_value=0;
	if(cf_skins_loaded.length>0)
	{

		cf_skins_loaded.forEach(function(skin)
		{
			if(cf_type=="host")
			{
				var style='<div class="deposit-skin-item coinflip_skin" onClick="" data-image="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" data-name="'+skin.market_hash_name+'" data-assetid="'+skin.assetid+'" data-appid="'+skin.appid+'" data-price="'+skin.price+'">\
								<img class="dsi-img" src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="'+skin.market_hash_name+'">\
								<div class="dsi-price">$'+skin.price+'</div>\
								<div class="dsi-name">'+skin.market_hash_name+'</div>\
						</div>';
				$('.coinflip-deposit-cont').prepend(style);
				total_value+=skin.price;
			}
			else
			{
				if(skin.price<=current_cf_max)
				{
					var style='<div class="deposit-skin-item coinflip_skin" onClick="" data-image="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" data-name="'+skin.market_hash_name+'" data-assetid="'+skin.assetid+'" data-appid="'+skin.appid+'" data-price="'+skin.price+'">\
								<img class="dsi-img" src="https://steamcommunity-a.akamaihd.net/economy/image/'+skin.image+'/60fx60f" alt="'+skin.market_hash_name+'">\
								<div class="dsi-price">$'+skin.price+'</div>\
								<div class="dsi-name">'+skin.market_hash_name+'</div>\
						</div>';
					$('.coinflip-deposit-cont').prepend(style);
					total_value+=skin.price;
				}
			}
		})
		total_value=round(total_value,2);
		$('.coinflip_deposit_total_text').html(''+cf_skins_loaded.length+' Total items ($'+total_value+')')
	}
});

/*
cf end
*/


$(document).on("click", ".redeemRefCodeBtn", function()
{
	var code=$('.redeemRefCodeInput').val();
	show_notification('info','Redeeming','Attempting to redeem code',5000);
	socket.emit('ref_redeem', {user: user, code: code}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Code redeeming error',removeXSS(err),5000);
		}
		else
		{
			show_notification('success','Code redeemed',removeXSS(result.message),5000);

		}
	});
});

$(document).on("click", ".saveRefCodeBtn", function()
{
	var code=$('.saveRefCodeInput').val();
	
	socket.emit('ref_save', {user: user, code: code}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Ref saving error',err,5000);
		}
		else
		{
			show_notification('success','Code saved','Successfully saved your code "'+removeXSS(result)+'"',5000);
			  
			  my_ref_link='https://dota2hunt.com/ref/'+result+'';
			  $('#referrals-display-code').val(''+result+'');
		}
	});
});


socket.on('notification', function(data)
{
	console.log('notification');
	console.log(data);
	show_notification(data.type,data.title,data.message,data.timeout);
});

socket.on('delete_message', function(data)
{
	$('div.chat-msgs-cont').find('div[data-messageid="'+data.message+'"]').remove();
});

socket.on('delete_messages', function(data)
{
	$('div.chat-msgs-cont').find('div[data-userid="'+data.userid+'"]').remove();
});

var my_ref_link='https://dota2hunt.com/ref/';
socket.on('update_ref', function(data)
{
	console.log(' ref data')
	console.log(data)
	$('.ref_users').html(data.users)
	$('.ref_balance').html('$'+round(data.balance,2))
	$('.ref_earned').html('$'+round(data.earned,2))
	$('.saveRefCodeInput').val(data.code)
	$('#referrals-display-code').val(''+data.code+'');
	my_ref_link='https://dota2hunt.com/ref/'+data.code+'';
	var reflevel=1;
	if(data.users>25)
	{
		reflevel=2;
		$('.afftier2').addClass('unlocked');
	}
	if(data.users>75)
	{
		reflevel=3;
		$('.afftier3').addClass('unlocked');
	}
	if(data.users>150)
	{
		reflevel=4;
		$('.afftier4').addClass('unlocked');
	}
	if(data.users>750)
	{
		reflevel=5;
		$('.afftier5').addClass('unlocked');
	}
	$('.reflevel').html(reflevel);
});

var cookie_ref_code=loadCookie('ref_code');
if(cookie_ref_code&&user._json)
{
	deleteCookie('ref_code');
	show_notification('info','Redeeming','Attempting to redeem code',5000);
	socket.emit('ref_redeem', {user: user, code: cookie_ref_code}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Code redeeming error',err,5000);
		}
		else
		{
			show_notification('success','Code redeemed',result.message,5000);

		}
	});
	history.pushState(null, '', '../');
	history.pushState(null, '', '');
}

function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function loadCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function deleteCookie(name) {
    createCookie(name, "", -1);
}



function remove_jp_skin(assetid)
{
	var tempArray = [];
	for (var i = 0; i < jackpot_skins_selected.length; i++)
	{
		if (jackpot_skins_selected[i] != null && jackpot_skins_selected[i].assetid != assetid)
		{
			tempArray.push(jackpot_skins_selected[i]);
		}
	}
	jackpot_skins_selected = tempArray;
}

function remove_cf_skin(assetid)
{
	var tempArray = [];
	for (var i = 0; i < coinflip_skins_selected.length; i++)
	{
		if (coinflip_skins_selected[i] != null && coinflip_skins_selected[i].assetid != assetid)
		{
			tempArray.push(coinflip_skins_selected[i]);
		}
	}
	coinflip_skins_selected = tempArray;
}


function round(value, decimals)
{
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

function floor(value, decimals) 
{
    return Number(Math.floor(value + 'e' + decimals) + 'e-' + decimals);
}


// Loading function
// insertLoadingCircle('.items-container')
function insertLoadingCircle(element){
    $(element).addClass('hideEverythingBelow');
    $(element).prepend('<div class="circle-loader"></div>');
    
    setTimeout(function(){
        $(element).children('.circle-loader').remove();
        $(element).removeClass('hideEverythingBelow');
    }, 1000);
}



// Jackpot circle code
//var jackpotColors = ['#bf3dd6','#4caf50','#d2ad56','#e91e63','#ff5722','#8d6e63','#27b09e'],
//    jackpotData = ['30','70'];
/*
    insertJackpotUsers(testAdder2);insertJackpotUser(testAdder);
    playersData[0].perc = 25;playersData[1].perc = 25;playersData[2].perc = 50;
*/

var jackpotColors = [],
    jackpotData = [],
    jackpotImg = [];

var config = {
	type: 'doughnut',
	data: {
		datasets: [{
			data: jackpotData,
			backgroundColor: jackpotColors,
		}],
	},
	options: {
        cutoutPercentage: 80,
		responsive: true,
        maintainAspectRatio: false,
        showLines: false,
        animation: {
            
        },
        hover: {
            intersect: false,
        },
        elements: {
            arc: {
                borderWidth: 0
            }
        },
        tooltips: {
            enabled: false
        },
//        plugins: {
//            labels: {
//                render: 'image',
//                images: jackpotImg
//            }
//        }
            pieceLabel: {
                render: 'image',
                images: jackpotImg
            }
    }
};

var ctx = document.getElementById('jackpot-chart').getContext('2d');
var jackpotChartjs = new Chart(ctx, config);

// Functions used in the animation below
var winnerData = {
        id: '3',
        steamid: '3',
        name: 'Geeeeeeergo4961',
        avatar: 'https://pbs.twimg.com/profile_images/1168578710259286023/a74uyYCx_400x400.jpg',
        skin_amount: 1,
        user_color: '#a542ea',
        chances: 25,
        total_value: '50'
    },
    playersData = [
        
    ],
    totalJackpotValue = 0;
// Jackpot winning animation function
// startJackpotRound(winnerData, playersData);


// ADDING IMAGES TEST





function removeSQL(str)
{
	if(!str)
	{
		return null;
	}
	if(str.length==0)
	{
		return null;
	}
	return str
      .replace(/\"/g, '&quot;')
      .replace(/\'/g, '&#39;')
}

function removeLinks(str,type)
{
	if(!str)
	{
		return str;
	}
	if(str.length==0)
	{
		return str;
	}
	if(type==1)
		var res = str.replace(/(https?:\/\/)?(www\.)?([-a-zA-Z0-9:%._\+~#=]{1,64})\.([a-z]{2,6})\b([-a-zA-Z0-9:%_\+.~#?&=]*)/ig, "LINK REMOVED");
	else
		var res = str.replace(/(https?:\/\/)?(www\.)?([-a-zA-Z0-9:%._\+~#=]{1,64})\.([a-z]{2,6})\b([-a-zA-Z0-9:%_\+.~#?&=]*)/ig, "");
	return res;
}

function removeXSS(str)
{
	if(!str)
	{
		return null;
	}
	if(str.length==0)
	{
		return null;
	}
	return str
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/\"/g, '&quot;')
      .replace(/\'/g, '&#39;')
      .replace(/\//g, '&#x2F;');
}
function removeXSS2(str)
{
	if(!str)
	{
		return null;
	}
	if(str.length==0)
	{
		return null;
	}
	return str
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/\"/g, '&quot;')
      .replace(/\'/g, '&#39;')
      .replace(/\//g, '&#x2F;');
}



function startJackpotRound(jackpotWinnerDegree)
{
    
    $('.jackpot-spinner').css('transition', 'transform 6.5s cubic-bezier(0.25, 1, 0.1, 1)');
    $('.jackpot-spinner').css('transform','rotate('+jackpotWinnerDegree+'deg)');
	
	setTimeout(function()
	{
		$('.signed-in-footer').html('<div class="footer-detail">\
										<div class="footer-value is_in_round">Not in Round</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Items:</div>\
										<div class="footer-value in_round_items">0</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Deposited:</div>\
										<div class="footer-value in_round_value">$0</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Chance:</div>\
										<div class="footer-value in_round_chance">0%</div>\
									</div>')
		$('.current-skins').html(0);
		$('.jp-round-skins').html('');
		$('.current-pot').html('$0');
		//$('.timer-text').html('Waiting for players <span class="awaiting-animation"></span>');
		$('.timer-text').fadeIn();
		$('.timer-starting').fadeOut();
		resetJackpotCircle();
		jackpot_animation=false;
		socket.emit('reset_jackpot', {potid: current_potid});
	},8000);
}
/*
function startJackpotRound(winnerData, playersData){
	console.log(winnerData);
	console.log(playersData);
	console.log(winnerData.id);
    // If any of the required arrays are missing, return an error
    if(winnerData == null || winnerData == undefined || winnerData == '' || winnerData == 0){
        console.error('[ERROR] Variable/Array missing');
        return;
    }
    if(playersData == null || playersData == undefined || playersData == '' || playersData == 0){
        console.error('[ERROR] Variable/Array missing');
        return;
    }
    // Local variables
    var jackpotTotalValue = 0,
        jackpotTotalPlayers = 0,
        jackpotWinnerDegree = {
            min: '',
            max: '',
        };
    
    // Finding the winning point
    // quick math: ([Users percent]   /   100) * 360

    var tempMin = 0,
        tempMax = 0;
	var i=0;
	for(var key in playersData) {
		console.log(playersData[key]);
        if(playersData[key].id != winnerData.id){
            // If the current user is NOT the winner
            tempMax += (parseFloat(playersData[key].chances) / 100) * 360;
            console.log(tempMax);
        } else {
            // If the current user is the winner
            tempMin = tempMax + 1;
            tempMax += (parseFloat(playersData[key].chances) / 100) * 360;
            console.log(tempMax);
        }
    }
	jackpotWinnerDegree = {
                min: tempMin,
                max: tempMax,
            };
    
    console.info('TEST');
    console.info(jackpotWinnerDegree);
    
    var jackpotSpinnerLocation = Math.floor(Math.random() * jackpotWinnerDegree.max) + jackpotWinnerDegree.min;
    jackpotSpinnerLocation = jackpotSpinnerLocation + (360 * (Math.floor(Math.random() * 9) + 5));
    
    $('.jackpot-spinner').css('transition', 'transform 6.5s cubic-bezier(0.25, 1, 0.1, 1)');
    $('.jackpot-spinner').css('transform','rotate('+jackpotSpinnerLocation+'deg)');
}*/

/*function startJackpotRound(winnerData, playersData){
	console.log(winnerData);
	console.log(playersData);
    // If any of the required arrays are missing, return an error
    if(winnerData == null || winnerData == undefined || winnerData == '' || winnerData == 0){
        console.error('[ERROR] Variable/Array missing');
        return;
    }
    if(playersData == null || playersData == undefined || playersData == '' || playersData == 0){
        console.error('[ERROR] Variable/Array missing');
        return;
    }
    // Local variables
    var jackpotTotalValue = 0,
        jackpotTotalPlayers = 0,
        jackpotWinnerDegree = {
            min: '',
            max: '',
        };
    
    // Finding the winning point
    // quick math: ([Users percent]   /   100) * 360

    var tempMin = 0,
        tempMax = 0;
    for(var i=0;i<playersData.length;i++){
		console.log(playersData[i])
		console.log(winnerData[i])
        if(playersData[i].id != winnerData.id){
            // If the current user is NOT the winner
            tempMax += (parseFloat(playersData[i].perc) / 100) * 360;
            console.log(tempMax);
        } else {
            // If the current user is the winner
            tempMin = tempMax + 1;
            tempMax += (parseFloat(playersData[i].perc) / 100) * 360;
            console.log(tempMax);
            jackpotWinnerDegree = {
                min: tempMin,
                max: tempMax,
            };
        }
    }
    
    console.info('TEST');
    console.info(jackpotWinnerDegree);
    
    var jackpotSpinnerLocation = Math.floor(Math.random() * jackpotWinnerDegree.max) + jackpotWinnerDegree.min;
    jackpotSpinnerLocation = jackpotSpinnerLocation + (360 * (Math.floor(Math.random() * 9) + 5));
    
    $('.jackpot-spinner').css('transition', 'transform 6.5s cubic-bezier(0.25, 1, 0.1, 1)');
    $('.jackpot-spinner').css('transform','rotate('+jackpotSpinnerLocation+'deg)');
}*/


// Adding a user function
var testAdder = {
        id: '5',
        steamid: '5',
        name: 'Geeeeeeergo4961',
        avatar: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/5a/5a22cccc638bf670081aefed5d8d4a0688bc74e3_full.jpg',
        skin_amount: 1,
        user_color: '#c34141',
        chances: 25,
        total_value: '0.60'
    };
var testAdder2 = [{
        id: '2',
        steamid: '2',
        name: 'Geeeeeeergo4961',
        avatar: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/5a/5a22cccc638bf670081aefed5d8d4a0688bc74e3_full.jpg',
        skin_amount: 1,
        user_color: '#4caf50',
        chances: 50,
        total_value: '50'
    },
    {
        id: '3',
        steamid: '3',
        name: 'abf',
        avatar: 'https://pbs.twimg.com/profile_images/1168578710259286023/a74uyYCx_400x400.jpg',
        skin_amount: 1,
        user_color: '#a542ea',
        chances: 50,
        total_value: '50'
    }];

//insertJackpotUser(testAdder);
function insertJackpotUser(userInfo){
    var playerhtml = '',
        existingPlayer = false;
    
    
    // Find out if the player is in the game
    for(var i=0;i < playersData.length;i++){
        if(userInfo.steamid == playersData[i].steamid){
            // The player is in the jackpot
            existingPlayer = true;
        }
    }
    
    
    // If the player is NOT in the jackpot

    if(existingPlayer == false){
        playersData.push({
			steamid: userInfo.steamid,
            id: userInfo.id,
            name: userInfo.name,
            avatar: userInfo.avatar,
            itemcount: userInfo.skin_amount,
            color: userInfo.user_color,
            perc: userInfo.chances,
            value: userInfo.total_value                   
        });
        
        jackpotChartjs.data.datasets[0].data.push(userInfo.total_value);
        jackpotChartjs.data.datasets[0].backgroundColor.push(userInfo.user_color);
        jackpotChartjs.options.pieceLabel.images.push({
            src: userInfo.avatar,
            width: 30,
            height: 30
        });
    
        playerhtml += '\
		<div class="deposit'+userInfo.steamid+'">\
            <div class="jackpot-entry-entry" data-pid="'+userInfo.steamid+'" style="border-left-color: '+userInfo.user_color+';height: 50px !important;">\
                <img class="jackpot-entry-avatar" src="'+userInfo.avatar+'">\
                <div class="jackpot-entry-text"><span class="jackpot-entry-name">'+removeLinks(removeXSS(userInfo.name))+'</span> deposited '+userInfo.skin_amount+' items</div>\
                <div class="jackpot-entry-value">+$'+parseFloat(userInfo.total_value).toFixed(2)+'</div>\
            </div>\
        </div>\
        ';
        
        if($('.waitingforplayers').length != 0){

            $('.jackpot-entry-entries').html('');
        }

        
        $('.current-jp-game .jackpot-entry-entries').prepend(playerhtml);
        
    } else if(existingPlayer == true){
        var pos = playersData.map(function(e) { return e.id; }).indexOf(userInfo.id);
        
        playersData[pos] = {
			steamid: userInfo.steamid,
            id: userInfo.id,
            name: userInfo.name,
            avatar: userInfo.avatar,
            itemcount: userInfo.skin_amount,
            color: userInfo.user_color,
            perc: userInfo.chances,
            value: userInfo.total_value                   
        };
        
        jackpotChartjs.data.datasets[0].data[pos] = userInfo.total_value;
        jackpotChartjs.data.datasets[0].backgroundColor[pos] = userInfo.user_color;
        /*
		jackpotChartjs.options.pieceLabel.images[pos] = {
            src: userInfo.avatar,
            width: 50,
            height: 50
        };
		*/
    
		
		
        playerhtml += '\
		<div class="deposit'+userInfo.steamid+'">\
            <div class="jackpot-entry-entry" data-pid="'+userInfo.steamid+'" style="border-left-color: '+userInfo.user_color+';height: 50px !important;">\
                <img class="jackpot-entry-avatar" src="'+userInfo.avatar+'">\
                <div class="jackpot-entry-text"><span class="jackpot-entry-name">'+removeLinks(removeXSS(userInfo.name))+'</span> deposited '+userInfo.skin_amount+' items</div>\
                <div class="jackpot-entry-value">+$'+parseFloat(userInfo.total_value).toFixed(2)+'</div>\
			</div>\
		</div>\
        ';
    
        //$('.current-jp-game .jackpot-entry-entries').prepend(playerhtml);
        $('.deposit'+userInfo.steamid+'').html(playerhtml);
    }
    
    jackpotChartjs.update();
}

function insertJackpotUsers(userInfo) {
    if(userInfo == undefined||userInfo == null||userInfo.length == {}){
        console.info('Error: User not found');
        console.info(userInfo);
        return;
    }
    
	var length=0;
    var playerhtml = '',
        imageHolder = [];
    
    
	for(var key in userInfo) {
		length++;
	}
	for(var key in userInfo) {
		var player = userInfo[key];
    // Updating the circle
        
        playersData.push({
			steamid: player.steamid,
            id: player.id,
            name: player.name,
            avatar: player.avatar,
            itemcount: player.skin_amount,
            color: player.user_color,
            perc: player.chances,
            value: player.total_value                   
        });
        
        //playersData[(length-1)].perc = player.chances;
        
        jackpotChartjs.data.datasets[0].data.push(player.total_value);
        jackpotChartjs.data.datasets[0].backgroundColor.push(player.user_color);
        //console.log(userInfo);
        //jackpotChartjs.options.plugins.labels.images.push({
        imageHolder.push({
        //jackpotImg.push({
            src: player.avatar,
            width: 30,
            height: 30
        });
        // Updating the game tab
        playerhtml += '\
		<div class="deposit'+player.steamid+'">\
            <div class="jackpot-entry-entry" data-pid="'+player.steamid+'" style="border-left-color: '+player.user_color+';height: 0px;">\
                <img class="jackpot-entry-avatar" src="'+player.avatar+'">\
                <div class="jackpot-entry-text"><span class="jackpot-entry-name">'+removeLinks(removeXSS(player.name))+'</span> deposited '+player.skin_amount+' items</div>\
                <div class="jackpot-entry-value">+$'+parseFloat(player.total_value).toFixed(2)+'</div>\
            </div>\
		</div>\
        ';
	
		if(player.steamid==my_steamid)
		{
			$('.is_in_round').html('In Round')
			$('.in_round_items').html(player.skin_amount)
			$('.in_round_value').html('$'+round(player.total_value,2))
			$('.in_round_chance').html(player.chances+'%')
		}
    }
    
    // Updating the game tab
//    jackpotChartjs.options.plugins.labels.images = imageHolder;
    jackpotChartjs.options.pieceLabel.images = imageHolder;
    
    $('.current-jp-game .jackpot-entry-entries').html(playerhtml);
    jackpotChartjs.update();
}

function resetJackpotCircle(){
	update(100,100);
    $('.jackpot-spinner').css('transform','rotate(0deg)');
    jackpotChartjs.data.datasets[0].data = [];
    jackpotChartjs.data.datasets[0].backgroundColor = [];
    playersData = [];
    imageHolder = [];
    jackpotChartjs.options.pieceLabel.images = [];
    totalJackpotValue = 0;
    jackpotChartjs.update();
    $('.current-jp-game .jackpot-entry-entries').html('<div class="jackpot-entry-entry jackpot-waiting">\
        <div class="jackpot-entry-text waitingforplayers">Waiting for players to join ...</div>\
    </div>')
}

var getObjectSize = function(obj) {
    var len = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) len++;
    }
    return len;
};

/*
function insertJackpotUsers(userInfo){
    var playerhtml = '';
    // Updating the circle
    playersData.forEach(function(player){
        totalJackpotValue += parseFloat(player.value);
        playersData[(playersData.length-1)].perc = ((player.value / totalJackpotValue) * 100);
        jackpotChartjs.data.datasets[0].data.push(playersData[(playersData.length-1)].perc);
        jackpotChartjs.data.datasets[0].backgroundColor.push(player.color);
        jackpotChartjs.options.plugins.labels.images.push({
            src: player.img,
            width: 50,
            height: 50
        });
        // Updating the game tab
        playerhtml += '\
            <div class="jackpot-entry-entry" data-pid="'+player.id+'" style="border-left-color: '+player.color+';height: 0px;">\
                <img class="jackpot-entry-avatar" src="'+player.img+'">\
                <div class="jackpot-entry-text"><span class="jackpot-entry-name">'+player.name+'</span> deposited '+player.itemcount+' items</div>\
                <div class="jackpot-entry-value">+$'+parseFloat(player.value).toFixed(2)+'</div>\
            </div>\
        ';
    });
    // Updating the game tab
    $('.current-jp-game .jackpot-entry-entries').html(playerhtml);
    jackpotChartjs.update();
}
*/

// Menu page switcher
$('.menu-item').click(function(){
    var pageid = $(this).attr('data-page');
	if(pageid=='highroller') return;
    $('.site-page').hide();
    $('.site-page[data-page="'+pageid+'"]').show();
    $('.menu-item').removeClass('active');
    $(this).addClass('active');
    
    // This might get removed
    if(pageid === '30max' || pageid === 'highroller') {
        resetJackpotCircle();
        $('.site-page').hide();
        $('.site-page[data-page="jackpot"]').show();
        insertLoadingCircle('.items-container');
        insertLoadingCircle('.jackpot-entries');
        insertLoadingCircle('.jackpot-holder');
        //$('.timer-text').html('Loading jackpot <span class="awaiting-animation"></span>');
		$('.timer-text').fadeIn();
		$('.timer-starting').fadeOut();
        setTimeout(function(){
//            $('.timer-text').html('Waiting for players <span class="awaiting-animation"></span>');
        }, 2000);
    } else if(pageid == 'coinflip'){
        insertLoadingCircle('.cag-body');
    }
    
});

$('.hsr-item-2').click(function(){
    $('.header-language-dropdown').slideToggle();
    
    console.info($('.hsr-item-2 i').css('transform'));
    
    if($('.hsr-item-2 i').css('transform') == 'matrix(1, 0, 0, 1, 0, 0)'){
        $('.hsr-item-2 i').css('transform', 'rotate(180deg)');
    } else {
        $('.hsr-item-2 i').css('transform', 'rotate(0deg)');
    }
});

$('.coinflip-tab').click(function(){
    var tabid = $(this).attr('data-tab');
    if(tabid === 'activecf'){
        $('.coinflip-tab').removeClass('active');
        $(this).addClass('active');
        $('.coinflip-body').fadeOut('fast');
        
        setTimeout(function(){
            $('.coinflip-current-stats').css('height', '100px');
            $('.coinflip-active-games').fadeIn('slow');
        }, 500);
    } else if(tabid === 'historycf'){
        $('.coinflip-tab').removeClass('active');
        $(this).addClass('active');
        $('.coinflip-body').fadeOut('fast');
        setTimeout(function(){
            $('.coinflip-current-stats').css('height', '0px');
            $('.coinflip-past-games').fadeIn('slow');
        }, 500);
    }
});


var store_selected=-1;

$(document).on('click', '.retransfer', function(e)
{
	var id=$(this).data('gameid')
	
		socket.emit('reset_sendback', {user:user, gameid: id}, function(err, result)
		{
			if(err)
			{
				show_notification('error','Retransfer error',err,5000);
				return;
			}
			show_notification('success','Successful retransfer','You have successfully initiated the retransfer.',5000);
		});
	
});



$(document).on('click', '.store_yes', function(e)
{
	if(store_selected!=-1)
	{
		$('#confirmation').modal('hide');
		socket.emit('purchase_skin', {user:user, assetid: store_selected}, function(err, result)
		{
			if(err)
			{
				show_notification('error','Store error',err,5000);
				return;
			}
			show_notification('success','Successful purchase','Successfully purchased the skin, it should be delivered to you soon',5000);
		});
	}
});

$(document).on('click', '.rsi-buy', function(e)
{
	var assetid=$(this).data('assetid');
	var price=$(this).data('price');
	var name=$(this).data('name');
	store_selected=assetid;
	$('.confirmation-txt').html('Are you sure you want to purchase the "<b>'+name+'</b>" skin for <b>$'+price+'</b>?')
    $('#confirmation').modal('show');
});

$(document).on('click', '.emoteToggle', function(e){
    $('.emote-cont').fadeToggle();
});

$(document).on('click', '.hsr-item-1', function(e){
	e.preventDefault();
    var id = $(this).attr('data-appid');

	if(id!=570&&id!=433850) return;
		
	current_appid=id;
	clearInterval(intervalTimer);
	jackpot_timer=false;
	jackpot_animation=false;
	$('.jackpot-history').html('');
	$('.jp-round-skins').html('');
	$('.signed-in-footer').html('<div class="footer-detail">\
										<div class="footer-value is_in_round">Not in Round</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Items:</div>\
										<div class="footer-value in_round_items">0</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Deposited:</div>\
										<div class="footer-value in_round_value">$0</div>\
									</div>\
									<div class="footer-detail">\
										<div class="footer-label">Chance:</div>\
										<div class="footer-value in_round_chance">0%</div>\
									</div>')
	
	$('.coinflip_lobbies').html('');
	$('.coinflip_recent').html('');
	
	//$('.timers-txt').html('Waiting for players <span class="awaiting-animation"></span>');
	
	if(current_loaded_page=='30max'&&id==570)
	{
		current_potid=1;
		socket.emit('switch_pots', {potid: 1});
	}
	
	if(current_loaded_page=='hr'&&id==570)
	{
		current_potid=2;
		socket.emit('switch_pots', {potid: 2});
	}
	
	if(current_loaded_page=='30max'&&id==433850)
	{
		current_potid=3;
		socket.emit('switch_pots', {potid: 3});
	}
	
	if(current_loaded_page=='hr'&&id==433850)
	{
		current_potid=4;
		socket.emit('switch_pots', {potid: 4});
	}
	
	for(var key in coinflip_games[current_appid])
	{
		var lobby=coinflip_games[current_appid][key];
		show_lobby(lobby);		
	}
    
    $('.hsr-item-1').removeClass('active');
    $(this).addClass('active');
    
    insertLoadingCircle('.cag-body');
    insertLoadingCircle('.items-container');
    insertLoadingCircle('.jackpot-entries');
    insertLoadingCircle('.jackpot-holder');
	
	update_coinflip_stats(current_appid);
	switch_page_top(current_loaded_page);
});

$('.modal').on('hidden.bs.modal', function () {
    e.preventDefault();
});





//circle start
let progressBar = document.querySelector('.e-c-progress');
let indicator = document.getElementById('e-indicator');
let length = Math.PI * 2 * 100;

progressBar.style.strokeDasharray = length;

function update(value, timePercent) {
  var offset = - length - length * value / (timePercent);
  progressBar.style.strokeDashoffset = offset; 
};

//circle ends

let intervalTimer;
let timeLeft;
let wholeTime = 60; // manage this to set the whole time 
let isPaused = false;
let isStarted = false;


/*
update(wholeTime,wholeTime);
displayTimeLeft(wholeTime);
*/

function changeWholeTime(seconds){
  if ((wholeTime + seconds) > 0){
    wholeTime += seconds;
    update(wholeTime,wholeTime);
  }
}

function timer (seconds){ //counts time, takes seconds


  let remainTime = Date.now() + (seconds * 1000);
  displayTimeLeft(seconds);
  
  
  $('.timer-text').hide();
  $('.timer-starting').fadeIn(); // timer-jackpot-txt
    
  intervalTimer = setInterval(function(){
    timeLeft = (remainTime - Date.now()) / 1000;

    $('.timer-jackpot-txt').html((timeLeft).toFixed(3) + 's');
    
    if(timeLeft <= 0){
      clearInterval(intervalTimer);
      isStarted = false;
      //displayTimeLeft(wholeTime);
      $('.timer-jackpot-txt').html('0.000s')
	  
	  setTimeout(function()
	  {
		$('.timer-text').fadeIn();
		$('.timer-starting').fadeOut();
	  },2000);
	  jackpot_timer=false;
	 
	  
      return ;
    }
    displayTimeLeft(timeLeft);
  }, 10);
}

function displayTimeLeft (timeLeft){ //displays time on the input
  let minutes = Math.floor(timeLeft / 60);
  let seconds = timeLeft % 60;
  
  update(timeLeft, wholeTime);
}

$(document).on('click', '.jackpot-entry-footer', function(e){
    var gameid = $(this).attr('data-gameid'),
        fairTab = $('.jackpot-entry-fair[data-gameid="'+gameid+'"]'),
        currentState = fairTab.attr('data-mode');
    
    if(currentState == 'closed'){
        fairTab.css('height', '200px');
        fairTab.attr('data-mode','opened');
    } else if(currentState == 'opened'){
        fairTab.css('height', '0px');
        fairTab.attr('data-mode','closed');
    }
});



$(document).on('keyup', '.oc-enter-code input', function(e){
    if (this.value.length == this.maxLength) {
      $(this).next('.oc-enter-code input').focus();
    }
});

function show_notification(type,title,text,timeout)
{
	if(!timeout) timeout=5000;
	var toast = VanillaToasts.create({
        title: title,
        text: text,
        type: type,
        timeout: timeout,
    });
}

function notTest(){
    var toast1 = VanillaToasts.create({
        title: 'Dota2Hunt',
        text: 'This is just a test',
        type: 'success',
        timeout: 5000,
    });
    var toast2 = VanillaToasts.create({
        title: 'Dota2Hunt',
        text: 'This is just a test',
        type: 'info',
        timeout: 5000,
    });
    var toast3 = VanillaToasts.create({
        title: 'Dota2Hunt',
        text: 'This is just a test',
        type: 'warning',
        timeout: 5000,
    });
    var toast4 = VanillaToasts.create({
        title: 'Dota2Hunt',
        text: 'This is just a test',
        type: 'error',
        timeout: 5000,
    });
}

//    $(".cf-coin-lobbyanimation-coin").animateSprite({
//		fps: 30,
//		autoplay: false,
//		animations: {
//			walkRight: animationArray,
//		},
//		loop: false,
//		complete: function(){
//            console.info('cf doneeee');
//		}
//	});
//    $(document).ready(function() {
//
//    });
var animationArrayOne = [0, 1];

	for (i = 2; i < 121; i++) {
		animationArrayOne.push(i);
	}

var animationSettingsCf = {
        fps: 30,
        autoplay: false,
        animations: {
            winner: animationArrayOne,
        },
        loop: false,
        complete: function(){
            console.info('cf doneeee');
        }
    },
    animationFirstPlayCf = true;
// 0 = Blue
// 1 = Red
function startCfAnimation(side){
    if(animationFirstPlayCf == true){
        $(".cf-coin-lobbyanimation-coin").animateSprite(animationSettingsCf);
    }
    $('.cf-countdown-ani').fadeIn();
    var cfCountDownTimer = 3000,
        cfLocker = false,
        cfCountDown = setInterval(function(){
            var timertext = parseFloat(cfCountDownTimer) / 1000;
            $('.cf-countdown-ani').html(timertext.toFixed(2) + 's');
            cfCountDownTimer = cfCountDownTimer - 10;
            if(cfCountDownTimer <= 0){
                $('.cf-countdown-ani').fadeOut();
                if(cfLocker == false){
                    cfLocker = true;
                    if(side == 1){ // Blue winner
        $(".cf-coin-lobbyanimation-coin").css('background-image', 'url("/assets/img/cf_blue_bait.png")');
        $(".cf-coin-lobbyanimation-coin").animateSprite('restart');
    } else if(side == 2){ // Red winner
        $(".cf-coin-lobbyanimation-coin").css('background-image', 'url("/assets/img/cf_red_bait.png")');
        $(".cf-coin-lobbyanimation-coin").animateSprite('restart');
    }
                }
                clearInterval(cfCountDown);
            }
        //cf-countdown-ani
    }, 10)
    
    
    

//    if(animationFirstPlayCf == true){
//        $(".cf-coin-lobbyanimation-coin").animateSprite(animationSettingBlue);
//        animationFirstPlayCf = false;
//    }else if(animationFirstPlayCf == false){
//        $(".cf-coin-lobbyanimation-coin").animateSprite('restart');
//    }
}
// insertLoadingCircle() 1000
$(document).on('click', '.history-nav-item', function(e){
    if($(this).hasClass('load_jackpot_data')){
        $('.mhct-head').fadeOut();
        $('.mhct-body').fadeOut();
        insertLoadingCircle('.mhct-body-cont');
        
        setTimeout(function(){
            $('.my-jackpot-history').fadeIn();
        }, 1000);
    }
    if($(this).hasClass('load_coinflip_data')){
        $('.mhct-head').fadeOut();
        $('.mhct-body').fadeOut();
        insertLoadingCircle('.mhct-body-cont');
        
        setTimeout(function(){
            $('.my-coinflip-history').fadeIn();
        }, 1000);
    }
    if($(this).hasClass('load_trade_data')){
        $('.mhct-head').fadeOut();
        $('.mhct-body').fadeOut();
        insertLoadingCircle('.mhct-body-cont');
        
        setTimeout(function(){
            $('.my-trade-history').fadeIn();
        }, 1000);
    }
	if($(this).hasClass('load_store_data')){
        $('.mhct-head').fadeOut();
        $('.mhct-body').fadeOut();
        insertLoadingCircle('.mhct-body-cont');
        
        setTimeout(function(){
            $('.my-store-history').fadeIn();
        }, 1000);
    }
	
	if($(this).hasClass('load_drop_data')){
        $('.mhct-head').fadeOut();
        $('.mhct-body').fadeOut();
        insertLoadingCircle('.mhct-body-cont');
        
        setTimeout(function(){
            $('.my-drop-history').fadeIn();
        }, 1000);
    }
});


var user_selected=-1;
var message_selected=-1;

// Chat right click menu
$(document).on("contextmenu", ".chat-msg", function () {
    //$('.msg').bind("contextmenu", function (event) {
	if(my_level<3&&is_admin<1) return
	
	var messageid=$(this).data('messageid');
	var userid=$(this).data('userid');
	user_selected=userid;
	message_selected=messageid;
	var extra='';
	if(is_admin>0)
	{
		extra+='<li data-action="mute">Mute User</li>\
				<li data-action="remove">Remove Message</li>\
				<li data-action="remove_all">Remove Messages</li>';
	}
	if(my_level>2)
	{
		extra+='<a href="https://steamcommunity.com/profiles/'+userid+'" target="_profile'+userid+'"><li data-action="view_profile">View Steam Profile</li></a>';
	}
	$('.chat-profile-menu').html(''+extra+' ');
    // Avoid the real one
    event.preventDefault();

    // Show contextmenu
    $(".chat-profile-menu").finish().toggle(100).

    // In the right position (the mouse)
    css({
        top: event.pageY + "px",
        left: event.pageX + "px"
    });
});


// If the document is clicked somewhere
$(document).bind("mousedown", function (e) {

    // If the clicked element is not the menu
    if (!$(e.target).parents(".chat-profile-menu").length > 0) {

        // Hide it
        $(".chat-profile-menu").hide(100);
    }
});


// If the menu element is clicked 
$(document).on("click", ".chat-profile-menu li", function () {

    // This is the triggered action name
    switch ($(this).attr("data-action")) {

        // A case for each action. Your actions here
        case "mute":
            
			var timer = prompt("Please enter a mute duration (minutes, -1 is perm)", "30");

			if (timer == null || timer == "")
			{
				return;
			}
			else
			{
				var reason = prompt("Please enter a reason for your mute (can leave it blank)", "");
				socket.emit('mute_user', {user: user, uid: user_selected, time: timer, reason: reason}, function(err, results)
				{
					if(err)
					{
						show_notification('error','Unable to Mute',err,5000);
						return;
					}
					else
					{
						show_notification('success','Success!','You\'ve successfully muted the user.',5000);
					}
					
				});				
			}
            break;
        case "remove":
			socket.emit('remove_message', {user: user, room: current_chatroom, message: message_selected}, function(err, results)
			{
				if(err)
				{
					show_notification('error','Unable to Remove message',err,5000);
					return;
				}
				else
				{
					show_notification('success','Success!','You\'ve successfully removed the message.',5000);
				}
			});		
            break;
        case "remove_all":
            socket.emit('remove_messages', {user: user, room: current_chatroom, uid: user_selected}, function(err, results)
			{
				if(err)
				{
					show_notification('error','Unable to Remove messages',err,5000);
					return;
				}
				else
				{
					show_notification('success','Success!','You\'ve successfully removed the messages.',5000);
				}
			});		
            break;
    }

    // Hide it AFTER the action was triggered
    $(".chat-profile-menu").hide(100);
});

$(document).on('click', '.adminPanelToggle', function () {
    $('.site-page').hide();
    $('.site-page[data-page="adminpanel"]').show();
});

$(document).on('click', '.adminmenu-item', function () {
    var page = $(this).attr('data-page');
    
    $('.adminmenu-item').removeClass('active');
    $(this).addClass('active');
    $('.adminpanel-page').hide();
    $('.adminpanel-page[data-page="'+page+'"]').show();
});


$(document).on('click', '.mobileChatToggle', function () {
    $('body').addClass('mobileChatOpened');
});

$(document).on('click', '.closeChatToggle', function () {
    $('body').removeClass('mobileChatOpened');
});

$(document).on('click', '.mobileMenuToggle', function () {
    $('body').addClass('mobileMenuOpened');
});
$(document).on('click', '.closeMenuToggle', function () {
    $('body').removeClass('mobileMenuOpened');
});

/*
Admin panel stuff
*/

$(document).on('click', '.rake_refresh', function ()
{
	var filter=$('.rake_filter').val();
	getRake(filter);	
});

$(document).on('click', '.adminPanelToggle', function ()
{
	getRake(1);	
});

$(document).on('click', '.adminmenu-item', function ()
{
    var page = $(this).attr('data-page');
	if(page=='statistics')
	{
		getRake(1);
	}
	if(page=='bots')
	{
		console.log('get_bots')
		getBots();
	}
	if(page=='items')
	{
		getPrices();
	}
	if(page=='players')
	{
		searchDB(1,0,1,0,0,0, function(err, results)
		{
			if(err)
			{
				show_notification('error','Admin Panel Error',err,5000);
				return;
			}
			else
			{
				handlePlayerQuery(results)
			}
		});
	}
	
	if(page=='coinflips')
	{
		searchDB(2,1,2,0,0,0, function(err, results)
		{
			if(err)
			{
				show_notification('error','Admin Panel Error',err,5000);
				return;
			}
			else
			{
				handleCFQuery(results);
			}
		});
	}
	
	if(page=='jackpots')
	{
		searchDB(3,2,2,6,1,0, function(err, results)
		{
			if(err)
			{
				show_notification('error','Admin Panel Error',err,5000);
				return;
			}
			else
			{
				handleJPQuery(results);
			}
		});
	}
	if(page=='trades')
	{
		searchDB(4,1,2,0,0,0, function(err, results)
		{
			if(err)
			{
				show_notification('error','Admin Panel Error',err,5000);
				return;
			}
			else
			{
				handleTradeQuery(results);
			}
		});
	}
	if(page=='support')
	{
		admin_load_tickets(2);
	}
});


$(document).on('click', '.jackpot_search_button', function ()
{
	var filter=$('.jackpot_filter').val();
	var filter_by=$('.jackpot_filter_by').val();
	var search=$('.jackpot_search').val();
	var searchquery=$('.jackpot_searchquery').val();
	console.log(filter,filter_by,search,searchquery);
	searchDB(3,filter,filter_by,search,searchquery,0, function(err, results)
	{
		if(err)
		{
			show_notification('error','Admin Panel Error',err,5000);
			return;
		}
		else
		{
			handleJPQuery(results);
		}
	});
});

$(document).on('click', '.coinflip_search_button', function ()
{
	var filter=$('.coinflip_filter').val();
	var filter_by=$('.coinflip_filter_by').val();
	var search=$('.coinflip_search').val();
	var searchquery=$('.coinflip_searchquery').val();
	console.log(filter,filter_by,search,searchquery);
	searchDB(2,filter,filter_by,search,searchquery,0, function(err, results)
	{
		if(err)
		{
			show_notification('error','Admin Panel Error',err,5000);
			return;
		}
		else
		{
			handleCFQuery(results);
		}
	});
});

$(document).on('click', '.player_search_button', function ()
{
	var filter=$('.player_filter').val();
	var filter_by=$('.player_filter_by').val();
	var search=$('.player_search').val();
	var searchquery=$('.player_searchquery').val();
	console.log(filter,filter_by,search,searchquery);
	searchDB(1,filter,filter_by,search,searchquery,0, function(err, results)
	{
		if(err)
		{
			show_notification('error','Admin Panel Error',err,5000);
			return;
		}
		else
		{
			handlePlayerQuery(results);
		}
	});
});

$(document).on('click', '.trade_search_button', function ()
{
	var filter=$('.trade_filter').val();
	var filter_by=$('.trade_filter_by').val();
	var search=$('.trade_search').val();
	var searchquery=$('.trade_searchquery').val();
	console.log(filter,filter_by,search,searchquery);
	searchDB(4,filter,filter_by,search,searchquery,0, function(err, results)
	{
		if(err)
		{
			show_notification('error','Admin Panel Error',err,5000);
			return;
		}
		else
		{
			handleTradeQuery(results);
		}
	});
});


function handleTradeQuery(results)
{
	var style='';
	results.forEach(function(trade)
	{
		style+=`<div class="table-row">
		
					<div class="table-bodyitem">
					  `+trade.id+`
					</div>
					
					<div class="table-bodyitem">
					  `+trade.type+` ($`+trade.value+`)
					</div>
					
					<div class="table-bodyitem">
					  `+trade.potid+`
					</div>
					
					<div class="table-bodyitem">
					  `+trade.offerid+`
					</div>
					
					<div class="table-bodyitem">
					  `+trade.userid+`
					</div>
					
					<div class="table-bodyitem">
					  `+trade.status+`
					</div>
					
					<div class="table-bodyitem">
					  `+trade.entered+`
					</div>
					
					<div class="table-bodyitem">
					  `+trade.hash+`
					</div>
					
				</div>`;
	})
	
	$('.trade_list').html(style);
}

function handlePlayerQuery(results)
{
	var style='';
	results.forEach(function(player)
	{
		style+=`<div class="table-row">
		
				<div class="table-bodyitem">
					  `+player.steamid+`
					</div>
					
					<div class="table-bodyitem">
						<img src="`+player.avatar+`" alt="player" class="table-playerimg"> &ensp; `+removeXSS(player.name)+`
					</div>
					
					<div class="table-bodyitem">
						`+removeXSS(player.trade_url)+`
					</div>
					<div class="table-bodyitem">
						`+player.admin+`
					</div>
					<div class="table-bodyitem">
						Bets: `+player.jackpot_bets+` times & `+player.jackpot_skins+` skins | Bet: $`+player.jackpot_amounts+` | Won: $`+player.jackpot_won+`
					</div>
					<div class="table-bodyitem">
						Bets: `+player.coinflip_bets+` times & `+player.coinflip_skins+` skins | Bet: $`+player.jackpot_amounts+` | Won: $`+player.jackpot_won+`
					</div>
					<div class="table-bodyitem">
						Ref Balance: $`+player.ref_balance+` ($`+player.earned+` total) | Code: `+player.ref_code+` | Users: $`+player.ref_users+` | Ref By: $`+player.ref_by+`
					</div>
				</div>`;
	})
	
	$('.player_list').html(style);
}
function handleCFQuery(results)
{
	var style='';
	results.forEach(function(coinflip)
	{
		var resend_text='';
		
		if(coinflip.status!="open")
		{
			if(coinflip.offer_status=='nomatch')
			{
				resend_text='&emsp; <i style="color:red;" class="fas fa-exclamation" title="This offer was most likely lost to a bot ban, please contact support to get a refund."></i> &emsp;';
			}
			if(coinflip.offer_status=='notrade-active'||coinflip.offer_status=='errored'||coinflip.offer_status=='expired'||coinflip.offer_status=='cancelled'||coinflip.offer_status=='declined')
			{
				resend_text='&emsp; <i style="color:lightgreen;" class="fas fa-sync pointer resend_button" data-type="coinflip" data-gameid="'+coinflip.id+'" data-potid="'+coinflip.appid+'" title="Click to resend this offer."></i> <font color="red">RESEND</font> &emsp;';
			}
		}
		var transfer_text='';
		var transfer_resend='';
		if(coinflip.hostvalue>=50)
		{
			if(coinflip.sendback_status!="accepted-set"||coinflip.sendback_status!="accepted-unset"||coinflip.sendback_status!="pending")
			{
				transfer_resend='<i style="color:lightgreen;" class="fas fa-sync pointer retransfer" data-gameid="'+coinflip.id+'"" title="Click to retransfer this offer."></i>';
			}
			transfer_text='&emsp; <b>Transfer:</b> '+coinflip.transfer_status+' | <b>Sendback:</b> '+coinflip.sendback_status+' ('+coinflip.sendback_attempts+') '+transfer_resend;
		}
		style+=`<div class="table-row">
		
				<div class="table-bodyitem">
					  `+coinflip.id+` | <b>`+coinflip.end_time.replace('Z', '').replace('T',' | ').replace('.000','')+`</b>
					</div>
					
					<div class="table-bodyitem">
						<img src="`+coinflip.hostavatar+`" alt="player" class="table-playerimg"> &ensp; `+removeXSS(coinflip.hostname)+` (`+coinflip.hostid+`) &ensp; <b>$`+coinflip.hostvalue+`</b>
					</div>
					
					<div class="table-bodyitem">
						<img src="`+coinflip.partavatar+`" alt="player" class="table-playerimg"> &ensp; `+removeXSS(coinflip.partname)+` (`+coinflip.partid+`) &ensp; <b>$`+coinflip.partvalue+`</b>
					</div>
					
					<div class="table-bodyitem">
						<img src="`+coinflip.winneravatar+`" alt="player" class="table-playerimg"> &ensp; `+removeXSS(coinflip.winnername)+` (`+coinflip.winnerid+`)
					</div>
					
					<div class="table-bodyitem">
						`+coinflip.module+` %
					</div>
					<div class="table-bodyitem">
						$`+coinflip.rake_taken+` | Ref: $`+coinflip.ref_bonus+`
					</div>
					<div class="table-bodyitem">
						Game Status: `+coinflip.status+` &ensp; Offer Status: `+coinflip.offer_status+` `+resend_text+` &ensp; Transfer: `+transfer_text+`
					</div>
				</div>`;
	})
	
	$('.coinflip_list').html(style);
}

function handleJPQuery(results)
{
	var style='';
	results.forEach(function(jackpot)
	{
		var resend_text='';
		

			if(jackpot.offer_status=='nomatch')
			{
				resend_text='&emsp; <i style="color:red;" class="fas fa-exclamation" title="This offer was most likely lost to a bot ban, please contact support to get a refund."></i> &emsp;';
			}
			if(jackpot.offer_status=='notrade-active'||jackpot.offer_status=='errored'||jackpot.offer_status=='expired'||jackpot.offer_status=='cancelled'||jackpot.offer_status=='declined')
			{
				resend_text='&emsp; <i style="color:lightgreen;" class="fas fa-sync pointer resend_button" data-type="jackpot" data-gameid="'+jackpot.gameid+'" data-potid="'+jackpot.potid+'" title="Click to resend this offer."></i> <font color="red">RESEND</font> &emsp;';
			}
		
		
		style+=`<div class="table-row">
		
				<div class="table-bodyitem">
					  `+jackpot.gameid+` (`+jackpot.potid+`) | <b>`+jackpot.end_time.replace('Z', '').replace('T',' | ').replace('.000','')+`</b>
					</div>
					
					<div class="table-bodyitem">
						<img src="`+jackpot.winneravatar+`" alt="player" class="table-playerimg"> &ensp; `+removeXSS(jackpot.winnername)+` (`+jackpot.winnerid+`) &emsp; <b>$`+jackpot.value+`</b>
					</div>
											
					<div class="table-bodyitem">
						`+round(jackpot.module*100,2)+` %
					</div>
					<div class="table-bodyitem">
						$`+jackpot.rake_taken+` | Ref: $`+jackpot.ref_bonus+`
					</div>
					<div class="table-bodyitem">
						`+jackpot.status+` `+resend_text+`
					</div>
				</div>`;
	})
	
	$('.jackpot_list').html(style);
}

function searchDB(database, filter, filter_by, search, searchquery, page, callback)
{
	socket.emit('search_db', {user: user, type: database, filter: filter, filter_by: filter_by, search: search, searchquery: searchquery, page: page}, function(err, result)
	{
		if(err)
		{
			callback(err);
		}
		else
		{
			callback(false, result);
		}
	});
}

function getRake(type)
{
	socket.emit('get_rake_stats', {user: user, type: type}, function(err, result)
	{
		if(err)
		{
			show_notification('error','Admin Panel Error',err,5000);
			return;
		}
		else
		{
			console.log(result);
			$('.pot-1-rake').html(result.rake.pot1);
			$('.pot-2-rake').html(result.rake.pot2);
			$('.pot-3-rake').html(result.rake.pot3);
			$('.pot-4-rake').html(result.rake.pot4);
			$('.h1-cf-rake').html(result.rake.h1cf);
			$('.d2-cf-rake').html(result.rake.dotacf);
			
			$('.pot-1-games').html(result.games.games1);
			$('.pot-2-games').html(result.games.games2);
			$('.pot-3-games').html(result.games.games3);
			$('.pot-4-games').html(result.games.games4);
			$('.h1-cf-games').html(result.games.h1cfgames);
			$('.d2-cf-games').html(result.games.dotacfgames);
			
			$('.pot-1-deposited').html(result.deposits.deposited1);
			$('.pot-2-deposited').html(result.deposits.deposited2);
			$('.pot-3-deposited').html(result.deposits.deposited3);
			$('.pot-4-deposited').html(result.deposits.deposited4);
			$('.h1-cf-deposited').html(result.deposits.depositedh1cf);
			$('.d2-cf-deposited').html(result.deposits.depositedd2cf);
			//show_notification('success','Success!','You\'ve successfully removed the messages.',5000);
		}
	});	
}

$(document).on('click', '.pricing_refresh', function ()
{
	getPrices();
});

var prices_loaded_d2=[];
var prices_loaded_h1=[];
function getPrices()
{
	socket.emit('get_prices', {user: user}, function(d2,h1)
	{
		if(1==2)
		{
			
		}
		else
		{
			prices_loaded_d2=d2;
			prices_loaded_h1=h1;
			
			var sort=$('.pricing_sort').val();
			var filter=$('.pricing_filter').val();
			
			var sort_prefix='-';
			if(sort==2)
			{
				sort_prefix='';	
			}
			var filter_prefix='name';
			if(filter==2)
			{
				filter_prefix='price';
			}
			
			prices_loaded_d2.sort(dynamicSort(''+sort_prefix+''+filter_prefix+''));
			prices_loaded_h1.sort(dynamicSort(''+sort_prefix+''+filter_prefix+''));
			
			console.log(prices_loaded_d2);
			console.log(prices_loaded_h1);
			
			var pages_d2=(floor(prices_loaded_d2.length/300,0))+1;
			var pages_h1=(floor(prices_loaded_h1.length/300,0))+1;
					
			
			var c_app=$('.pricing_api').val();
			
			
			var thisarray=prices_loaded_d2;
			if(c_app==433850)
			{
				thisarray=prices_loaded_h1;
			}
			
			addPricesPagination(thisarray);	
			show_admin_skins(thisarray,1);			
		}
	});	
}

var prices_current_page=1;
$(document).on("click", ".spec_page", function()
{
	var page=$(this).data('page');
	prices_current_page=page;
	$('.appp-picker').removeClass('active');
	
	var thispage=$(`.spec_page[data-page=${page}]`);
	thispage.addClass('active');
	
	var c_app=$('.pricing_api').val();			
	var thisarray=prices_loaded_d2;
	if(c_app==433850)
	{
		thisarray=prices_loaded_h1;
	}
	
	show_admin_skins(thisarray,page);
});

function show_admin_skins(array,page)
{
	$('.admin_items').html('');
	var temp=paginate(array,300,page);	

	temp.forEach(function(skin)
	{
		var style=`<div class="table-row">
						<div class="table-bodyitem">
							$`+skin.price+`
						</div>
						<div class="table-bodyitem skincell">
							`+skin.name+`
						</div>
						<div class="table-bodyitem">
							<div class="table-btn">Edit Price</div>
						</div>
					</div>`;
				
		$('.admin_items').append(style);
	});		
}

function paginate (array, page_size, page_number)
{
  --page_number; // because pages logically start with 1, but technically with 0
  return array.slice(page_number * page_size, (page_number + 1) * page_size);
}

function addPricesPagination(array)
{
	var pages=(floor(array.length/300,0))+1;
	
	var page_html='';
	for(var i = 1; i <= pages; i++)
	{
		var bonus='';
		if(i==1) bonus='active';
		page_html+='<div class="appp-picker '+bonus+' spec_page" data-page="'+i+'">'+i+'</div>';
	}
	
	var style=`
						`+page_html+`
				`;
				
	$('.pricing_pages').html(style);
}


function dynamicSort(property){
	var sortOrder = 1;
	if(property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1);
	}
	return function (a,b) {
		/* next line works with strings and numbers, 
		 * and you may want to customize it to your needs
		 */
		var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
		return result * sortOrder;
	}
}

function getBots()
{
	socket.emit('get_bots', {user: user}, function(bot1,bot2,bot3,bot4,cfbot,storagebot,vaultbot)
	{
			console.log(bot1);
			$('.site_bots').prepend(`<div class="table-row">
					<div class="table-bodyitem">
						`+bot1.steamid+`
					</div>
					<div class="table-bodyitem">
						JP 1
					</div>
					<div class="table-bodyitem">
						`+bot1.username+`
					</div>
					<div class="table-bodyitem">
						`+bot1.trade_url+`
					</div>
				</div>`);
			$('.site_bots').prepend(`<div class="table-row">
					<div class="table-bodyitem">
						`+bot2.steamid+`
					</div>
					<div class="table-bodyitem">
						JP 2
					</div>
					<div class="table-bodyitem">
						`+bot2.username+`
					</div>
					<div class="table-bodyitem">
						`+bot2.trade_url+`
					</div>
				</div>`);
			$('.site_bots').prepend(`<div class="table-row">
				<div class="table-bodyitem">
					`+bot3.steamid+`
				</div>
				<div class="table-bodyitem">
					JP 3
				</div>
				<div class="table-bodyitem">
					`+bot3.username+`
				</div>
				<div class="table-bodyitem">
					`+bot3.trade_url+`
				</div>
			</div>`);
			$('.site_bots').prepend(`<div class="table-row">
				<div class="table-bodyitem">
					`+bot4.steamid+`
				</div>
				<div class="table-bodyitem">
					JP 4
				</div>
				<div class="table-bodyitem">
					`+bot4.username+`
				</div>
				<div class="table-bodyitem">
					`+bot4.trade_url+`
				</div>
			</div>`);
			
			$('.site_bots').prepend(`<div class="table-row">
				<div class="table-bodyitem">
					`+cfbot.steamid+`
				</div>
				<div class="table-bodyitem">
					CF Bot
				</div>
				<div class="table-bodyitem">
					`+cfbot.username+`
				</div>
				<div class="table-bodyitem">
					`+cfbot.trade_url+`
				</div>
			</div>`);
			
			$('.site_bots').prepend(`<div class="table-row">
				<div class="table-bodyitem">
					`+storagebot.steamid+`
				</div>
				<div class="table-bodyitem">
					Storage Bot
				</div>
				<div class="table-bodyitem">
					`+storagebot.username+`
				</div>
				<div class="table-bodyitem">
					`+storagebot.trade_url+`
				</div>
			</div>`);
			
			$('.site_bots').prepend(`<div class="table-row">
				<div class="table-bodyitem">
					`+vaultbot.steamid+`
				</div>
				<div class="table-bodyitem">
					Vault Bot
				</div>
				<div class="table-bodyitem">
					`+vaultbot.username+`
				</div>
				<div class="table-bodyitem">
					`+vaultbot.trade_url+`
				</div>
			</div>`);
		
	});	
}